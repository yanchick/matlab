function net=net3learn(x,y,k1,n1)
x=x;
y=y;
xi=[];Q=[];


for j=1:k1
n=length(x);
z=[zeros(1,j) x(1:n-j)];
xi=[xi; z];
Q=[Q ;min(x) max(x)];
end

f=[n1 3];
net3=newff(Q,f,{'tansig' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs = 2500;
net3.trainParam.goal = 1e-7;
net3.trainParam.min_grad=1e-20;
net3=train(net3,xi,y);
net=net3


end