clear all;
dt=5e-4;%Шаг интегрирования
T0=5;%Время интегрирования
Nstep=1000;%Объем памяти
Jk=3000;%Точка переключения
T=0:dt:T0;
N=length(T);
H=1;
A=0.1;
B=0.05;
F=[0  -H/A 0;
   H/B   0 0;
   0     1 0;];%Матрица состояния
G=[1; 0 ;0]; %Матрица Управления
C=[0 0 1];  %Матрица наблюдения
w0=5;w1=10;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
%Формирования случайных величин
Ma=0.4;%СКО момента
Umax=1*0.1*pi/(180*60);%СКО шума
%Измерительный Шум
v1=Umax*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Umax/st);
v=v1;
clear v1;
%Форимирование случайного воздействия по оси ГС
v1=Ma*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Ma/st);
w=v1;
clear v1 v2;
%Формирования корреляционых матриц
Q=Umax^2; 
W=Ma^2;
Sig=zeros(3);
%Решение уравнения Риккати
dt0=0.002;
Sigp=[];
for t=0:dt0:0.3
   dSig=F*Sig+Sig*transpose(F)-Sig*transpose(C)*inv(Q)*C*Sig+G*W*transpose(G);  
   Sig=Sig+dSig*dt0;
   Sigp=[Sigp Sig(1,1)];
end
K=Sig*transpose(C)*inv(Q);
%Моделирование Системы
x=zeros(3,1);ex=zeros(3,1);
i=1;
OutX=[];OutEx=[];q=[];West=[];Sig_est=Sig;Wvar=W;
Jmp=5;xlast=0;Sr=[];alx=0;Shum=[];Wful=[];F1=[];F2=[];
als=[];
for t=0:dt:T0
 u=-P*ex;%Управление
  y=C*x+v(i);%Наблюдение  
  Delt_x=y-C*ex;%Разность между выходом фильтра и измерен
  dx=F*x+G*(u+Jmp*w(i));%Основная модель 
  dex=F*ex+G*u+K*Delt_x;%Фильтр
  x=x+dx*dt;%Перменная состояния объекта
  exlast=ex;
  ex=ex+dex*dt;%Переменная состояния фильтра
  %Эталонная модель
  detx=F*ex+G*u;%Основная модель  
  %Определение праметров наблюдения
  F1=[F1 F*x+G*u];
  F2=[F2 F*ex+G*u];
  q=[1 0 0]*(y); %текущее значение шума
  q1=[1 0 0]*(dx-F*x-G*u);
  West=[West q]; %Запоминаем шум в память
  Wful=[Wful q1];
  %if (i==Jk)
  % Jmp=8;    
  %end
%{
if (mod(i,Nstep)==0)
      Wvar=var(West); %Оценка Шума
      Wvar2=var(Wful);
      Sr=[Sr [Wvar;Wvar2]];
      West=[];
      Wful=[];
 %     K=Sig_est*transpose(C)*inv(Q);  
 end
  xlast=ex;
   dSig=F*Sig_est+Sig_est*transpose(F)-Sig_est*transpose(C)*inv(Q)*C*Sig_est+G*Wvar*transpose(G);  %Решение динамического уравнения риккати
   Sig_est=Sig_est+dSig*dt;
   Sigp=[Sigp Sig_est(1,1)];
%}
OutX=[OutX x];
OutEx=[OutEx ex];
Shum=[Shum Jmp*w(i)];
i=i+1;
end
plot (T,Shum(1,:),T,rad2deg(OutX(3,:))*60); figure(gcf)
