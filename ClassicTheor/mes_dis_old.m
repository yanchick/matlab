%�������� ������ �� "��������������� ����������� �����"
%�������� �������� ������
t=load('rd.mat');
dataForm;
nn=8;%������� �������
dt=1e-6;%��� ��������������
T0=0.1;%����� ��������������
T=0:dt:T0;
N=length(T);
dataForm;
%������ ����������
%�������� �������� �������������
w0=5;w2=20;w3=25;w4=40;ksi=0.9;
goal=sort([desir(w0,ksi) desir(w2,ksi) desir(w3,ksi) desir(w4,ksi)]);
P=place(A8,B8,goal);
Rikatti;
%������������� �� ��������
G1=G;
[A B C D]=ssdata(Stabil);
sx=[];
step=0.1;
for sl=2:1:100    
ds=step*sl;    
G=ds*G1;
%������ ������� ���������� ��� ����� ����
Dzer=zeros(2,2);
Deye=eye(2);
D=[Dzer Deye;Dzer Dzer];
Gs8=ss(A,[B 0*B],[C;C],D,'inputname',{'Mom1','Mom2','Noise1','Noise2'},'outputname',{'Alpha_Noisy','Beta_Noisy','tAlpha','tBeta'});%������� ��� Simulink
%������ �������
Aest=[A-G*C-B*P];
Best=G;
Cest=[zeros(2,8); eye(8)];
Dest=[Deye; zeros(8,2)];
Kalman=ss(Aest,Best,Cest,Dest,'inputname',{'A1','B2'},'outputname',{'mesAlpha','mesBeta','Alpha','Beta','intAlpha','intBeta','pAlpha','pBeta','plAlpha','plBeta'});
%�����������
sys=series(Gs8,Kalman,[1 2],[1 2]);
SimModel=feedback(sys,P,[1 2],[3 4 5 6 7 8 9 10]);
SimModel=SimModel([1 2 3 4],[1 2 3 4]);
[out,x]=lsim(SimModel,[w' v'],T);
ClearSignal=(out(:,1)-v(1,:)');
sx=[sx [rad2deg(std(out(:,3)-ClearSignal))*60*60*10;ds]];
end

%������������ � ����� ��������
%{
[some Cs Ns]=Modeling(Gs8,T,P,G,v,w);
[c i]=min(some(1,:))
plot(some(2,:),some(1,:));
grid on;
xlabel(' ');
ylabel(' ');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print('-djpeg','-r300','D1.jpg');

figure;
plot(T,Cs);
grid on;
xlabel(' ');
ylabel(' ');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print('-djpeg','-r300','Clear.jpg');

figure;
plot(T,Cs);
grid on;
xlabel(' ');
ylabel(' ');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print('-djpeg','-r300','Filtered.jpg');

figure;
[some Cs Ns]=Modeling(Gs8,T,P,G,2*v,w);
[c i]=min(some(1,:))
plot(some(2,:),some(1,:));
grid on;
xlabel(' ');
ylabel(' ');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print('-djpeg','-r300','D2.jpg');
figure;
[some Cs Ns]=Modeling(Gs8,T,P,G,0.5*v,w);
[c i]=min(some(1,:))
plot(some(2,:),some(1,:));
grid on;
xlabel(' ');
ylabel(' ');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print('-djpeg','-r300','D05.jpg');
%}