z=1;
for k1=4:10
    for n1=4:10
        for n2=4:10
            if isempty(Q{k1,n1,n2})==0
                for al2=0:4:16
                    for al3=0:4:16
                        if z>496
                            net1=Q{k1,n1,n2}.net1;
                            net2=Q{k1,n1,n2}.net2;
                            net3=Q{k1,n1,n2}.net3;
                            [X,Y]=ThreeAxisNonlinear(0,al2,al3,k1,net1,net2,net3);
                            Result.k=k1;
                            Result.n1=n1;
                            Result.n2=n2;
                            Result.x=X;
                            Result.y=Y;
                            Result.al2=al2;
                            Result.al3=al3;
                            Result.net1=net1;
                            Result.net2=net3;
                            Result.net3=net3;
                            R{z}=Result;
                            save kmu11_5_1.mat R
                        end
                        z=z+1
                    end
                end
            end
            
        end
    end
end
% % % [net1,net2,net3]=ThreeAxisNonlinear_learn(4,10,7);
% % [X,Y]=ThreeAxisNonlinear(0,0,0,4,net1,net2,net3);
% mx=max(abs(x'))';
% mdn=mean(x')';
% [a b]=size(x);
% tpp=[];
% for j=1:a
% for i=length(x):-1:1
%     if abs(x(j,i)-mdn)>0.05*mdn
%         tpp=[tpp i];
%         break
%     end
% end
% end
% for i=1:343
%     if isempty(R{i})==0
%         
%         [mx,mdn,tpp]=GetpEstPP(R{i}.x);        
%         if max(abs(mx))<3e2   
%             Q{R{i}.k,R{i}.n1,R{i}.n2}=R{i};
%             Q{R{i}.k,R{i}.n1,R{i}.n2}.max=mx;
%             Q{R{i}.k,R{i}.n1,R{i}.n2}.mean=mdn;
%             Q{R{i}.k,R{i}.n1,R{i}.n2}.time=tpp;
%         
%         end
%     end
% end
% PP=[];
% for i=1:10
%     for j=1:10
%         for k=1:10
%             if isempty(Q{i,j,k})==0
%              PP=[PP [Q{i,j,k}.max;Q{i,j,k}.mean;Q{i,j,k}.time;i;j;k]];   
%              
%             end
%         end
%     end
% end

% 
% for j=1:39
%     L{PP(10,j)}=[];
% end
% for j=1:39
%     if PP(7,j)~=2001
%     L{PP(10,j)}=[L{PP(10,j)} PP(7,j)];    
%     end
% end
% x=[];
% y=[];
% e=[];
% for j=1:10
%     if isempty(L{j})==0 
%      x=[x j];
%      y=[y mean(L{j})];
%      e=[e (max(L{j})-min(L{j}))/2];
%     end
% end
%PP=PP0;
% for i=length(PP):-1:1
%     if PP(7,i)>3900
%         PP(:,i)=[]
% 
%     end
% end
% PQ=PP;
% PQ(2:3:9,:)=[];
% PQ(2:2:6,:)=[];
% 
% [XI,YI] = meshgrid(4:0.1:10);
% ZI1=griddata(PP(10,:),PP(11,:),PP(1,:),XI,YI,'cubic');
% surf(XI,YI,ZI1);
% text(10,3,0,'m','FontSize',20)
% text(3,10,0,'n_1','FontSize',20)
% print -dpng max_k_n1.png 
% ZI2=griddata(PP(10,:),PP(11,:),PP(4,:),XI,YI,'cubic');
% surf(XI,YI,ZI2);
% text(10,3,0,'m','FontSize',20)
% text(3,10,0,'n_1','FontSize',20)
% print -dpng mean_k_n1.png 
% ZI3=griddata(PP(10,:),PP(11,:),PP(7,:),XI,YI,'cubic');
% surf(XI,YI,ZI3);
% text(10,3,0,'m','FontSize',20)
% text(3,10,0,'n_1','FontSize',20)
% print -dpng time_k_n1.png 
% 
% ZI1=griddata(PP(10,:),PP(12,:),PP(1,:),XI,YI,'cubic');
% surf(XI,YI,ZI1);
% text(10,3,0,'m','FontSize',20)
% text(3,10,0,'n_2','FontSize',20)
% print -dpng max_k_n2.png 
% ZI2=griddata(PP(10,:),PP(12,:),PP(4,:),XI,YI,'cubic');
% surf(XI,YI,ZI2);
% text(10,3,0,'m','FontSize',20)
% text(3,10,0,'n_2','FontSize',20)
% print -dpng mean_k_n2.png 
% ZI3=griddata(PP(10,:),PP(12,:),PP(7,:),XI,YI,'cubic');
% surf(XI,YI,ZI3);
% text(10,3,0,'m','FontSize',20)
% text(3,10,0,'n_2','FontSize',20)
% print -dpng time_k_n2.png 
% 
% ZI1=griddata(PP(11,:),PP(12,:),PP(1,:),XI,YI,'cubic');
% surf(XI,YI,ZI1);
% text(10,3,0,'n_1','FontSize',20)
% text(3,10,0,'n_2','FontSize',20)
% print -dpng max_n1_n2.png 
% ZI2=griddata(PP(11,:),PP(12,:),PP(4,:),XI,YI,'cubic');
% surf(XI,YI,ZI2);
% text(10,3,0,'n_1','FontSize',20)
% text(3,10,0,'n_2','FontSize',20)
% print -dpng mean_n1_n2.png 
% ZI3=griddata(PP(11,:),PP(12,:),PP(7,:),XI,YI,'cubic');
% surf(XI,YI,ZI3);
% text(10,3,0,'n_1','FontSize',20)
% text(3,10,0,'n_2','FontSize',20)
% print -dpng time_n1_n2.png 

% surf(XI,YI,ZI);
% 
% 
% WW=[];
% for i=1:975
%     [mx,mdn,tpp]=GetpEstPP(R{i}.x); 
%     Wt=[mx;mdn;tpp;R{i}.al2;R{i}.al3];
%     WW=[WW Wt];
% end

% for i=1:1:31
%     for j=1:1:31
%         if isnan(ZI3(i,j))==1
%             ZI3(i,j)=(ZI3(i-1,j)+ZI3(i,j-1))/2;
%         end 
%     end
% end
%surf(XI,YI,ZI3);
%set(gcf,'PaperUnits','inches','PaperPosition',[0 0 4 3])
% plot(0:5e-3:5e-3*1000,ans(1,1:1001),'LineWidth',4);
% grid
% text(5e-3*1070,-0.2,'t','FontSize',20)
% text(-0.2,8.5,'\alpha','FontSize',20)
% print -dpng time.png 