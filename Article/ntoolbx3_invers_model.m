%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Моделирование, одна сеть
%======================================================================

dig=16;%Разрядность
jd=2^dig;
net30=net3;

net3=net30;

net3.IW{1,1}=round(net30.IW{1,1}.*jd)./jd;
net3.LW{2,1}=round(net30.LW{2,1}.*jd)./jd;
net3.b{1}=round(net30.b{1}.*jd)./jd;
net3.b{2}=round(net30.b{2}.*jd)./jd;

dt=0.01;
Tend=20;
x0=zeros(k3,1);
x=x0;
xz=x;
sqerr=[];sqerr2=[];
InNet1=zeros(k1_1,1);
InNet2=zeros(k2_1,1);
alpha=[0];
alphapr=0;
t1=0:dt:Tend;
u0=0;
xest=u0;
xwcap=[0;0;0];
Sqerr=[0;0;0];
  
for t=0:dt:Tend;
    w=1;
    tin1=InNet1;
    tin2=InNet2;
    
    u=xest;
    dx=F*x+G*u+G*w;   
    x=x+dx*dt;
    xz=[xz x];
    y=x(3);
    
    tin1=[y; tin1];
    tin2=[u; tin2];
    
    InNet1=tin1(1:k1_1);
    InNet2=tin2(1:k2_1);
    
    tin1=[];
    tin2=[];
    xest=sim(net3,[InNet1;InNet2]);
    Sqerr=[Sqerr sqrt((xest-x).^2)];

    sqerr=[sqerr xest];   
    alphapr=alphapr+x(1)*dt;
    alpha=[alpha ;alphapr];
end
%lk=2;
%plot(t1,sqerr(lk,1:min(length(sqerr),length(xz))),t1,xz(lk,1:min(length(sqerr),length(xz))));
%}