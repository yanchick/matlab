k1_1=10;%Количество слоев задержки по состоянию
k2_1=0;%Количество слоев задержки по упраавлению
k3=3;%Порядок системы
k1=k1_1;


dt=0.01;
Tend=60;
x0=zeros(k3,1);
x=x0;
xz=x;
sqerr=[];sqerr2=[];

ksi=0.7;
w0=1*2*pi;
H=0.12;
A=0.016;
B=0.0005;
h=0.05;
F7=[h/(7*A)  -0*H/A 0;
   H/B   0 0;
   0     1 0;];

F4=[h/(4*A)  -0*H/A 0;
   H/B   0 0;
   0     1 0;];

F1=[h/(A)  -0*H/A 0;
   H/B   0 0;
   0     1 0;];


G=[1; 0 ;0];
C=[0 0 1];  
D=zeros(3,1);
w0=1;w1=3;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P7=place(F7,G,goal);
P4=place(F4,G,goal);
P1=place(F1,G,goal);
% Umax=0.1*deg2rad(1/60);
% Ma=1; 
% Q=[Umax^2]; 
% W=[Ma^2]; 
% Sig=zeros(3);
% dt0=5e-4;
% Sigp=[];
% for t=0:dt0:0.5
%    dSig=F*Sig+Sig*transpose(F)-Sig*transpose(C)*inv(Q)*C*Sig+G*W*transpose(G);  
%    Sig=Sig+dSig*dt0;
%    Sigp=[Sigp Sig(1,1)];
% end
% 
% K=Sig*transpose(C)*inv(Q);

a=-1;
b=1;
InNet1=zeros(k1,1);InNet4=zeros(k1,1);InNet7=zeros(k1,1);
alpha=[];alpha1=[0];alpha2=[0];
alphapr=[0;0;0];u0=0;xest=x0;xk=x0;
xwcap=[0;0;0];Sqerr=[0;0;0];
x0=zeros(k3,1);
x7=x0;xz7=x7;xest7=x7;
x4=x0;xz4=x4;xest4=x4;
x1=x0;xz1=x1;xest1=x1;

for t=0:dt:Tend;
     w=0.5;
    tin7=InNet7;
    u7=-P7*xest7;
    dx7=F7*x7+G*u7+G*w+[((4*A)/8-(1*A)/8)*x4(1)*x1(1);0;0];   
    x7=x7+dx7*dt;
    xz7=[xz7 x7];
    y7=x7(3)+5*deg2rad(1/60)*randn(1);    
    tin7=[y7; tin7];    
    InNet7=tin7(1:k1);
    xest7=sim(net01,[InNet7]);
   
    tin4=InNet4;
    u4=-P4*xest4;
    dx4=F4*x4+G*u4+G*w+[((1*A)/8-(7*A)/8)*x1(1)*x7(1);0;0];  
    x4=x4+dx4*dt;
    xz4=[xz4 x4];
    y4=x4(3)+5*deg2rad(1/60)*randn(1);    
    tin4=[y4; tin4];    
    InNet4=tin4(1:k1);
    xest4=sim(net03,[InNet4]);
   
    tin1=InNet1;
    u1=-P1*xest1;
    dx1=F1*x1+G*u1+G*w+[((7*A)/8-(4*A)/8)*x7(1)*x4(1);0;0];      
    x1=x1+dx1*dt;
    xz1=[xz1 x1];
    y1=x1(3)+5*deg2rad(1/60)*randn(1);    
    tin1=[y1; tin1];    
    InNet1=tin1(1:k1);
    xest1=sim(net03,[InNet1]);

%    alphapr=alphapr+[x4(1)]*dt;
    alphapr=alphapr+[x7(1);x4(1);x1(1)]*dt;
    alpha=[alpha rad2deg(alphapr)*60];
end