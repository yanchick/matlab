%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Расчет, одна сеть
%======================================================================
clear
t=0:0.01:20*pi;

%w=0.1*randn(length(t),1)';
w=sin(2*pi*0.5*t);
ksi=0.89;
w0=1*2*pi;
H=0.01;
A=0.16;
B=0.0005;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];
G=[1; 0 ;0];
C=[0 0 1];
D=0;
w0=0.5;w1=0.9;ksi=0.8;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
Gs=ss(F,G,C,D);
Q=10;
W=0.1;
[L,K]=kalman(Gs,Q,W);
eig(F-K*C)


x1=0;x2=0;x3=0;
xf=[];xw=[];
xk=zeros(3,1)
x=zeros(3,1)
u=-P*xk;
dt=1e-3;Tend=50;
for t=0:dt:Tend;
  w=W*rand(1);        
  dx=F*x+G*u+G;
  x=x+dx*dt;
  y=C*x+Q*rand(1);
  Delt_x=y-C*xk;
  dxk=(F-K*C)*xk+K*y+G*u;
  xk=xk+dxk*dt;
  u=-P*xk;   
%  x=[x1;x2;x3];
  xf=[xf xk];    
  xw=[xw x];
end
