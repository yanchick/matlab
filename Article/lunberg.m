clear all;
dt=1e-3;%Шаг интегрирования
T0=10;%Время интегрирования
T=0:dt:T0;

N=length(T);
H=0.12;
A=0.016;
B=8e-5;

F=[0  -H/B 0;     %альфа'
   H/A   0 0;     %бета'
   0     1 0;];   %бета
G=[1; 0 ;0];
C=[0 0 1];  
D=zeros(3,1);

w0=3;w1=1;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);

w0=20;w1=25;ksi=0.85;
goal=sort([desir(w0,ksi) -1/w1]);

L=place(F',C',goal)';

%Формирования случайных величин
Ma=1;%СКО момента
Umax=1*0.1*pi/(180*60);%СКО шума
%Моделирование Системы

%
v1=Umax*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Umax/st);
v=v1;
clear v1;
%
v1=Ma*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Ma/st);
w=v1;
clear v1 v2;

Q=Umax^2; 
W=Ma^2;
Sig=zeros(3);

x=zeros(3,1);ex=zeros(3,1);
i=1;
OutX=[];OutEx=[];q=[];West=[];Sig_est=Sig;Wvar=W;
Jmp=5;xlast=0;Sr=[];alx=0;Shum=[];Wful=[];F1=[];F2=[];
als=[];

for t=0:dt:T0
 u=-P*ex;
  y=C*x+v(i); 
  Delt_x=y-C*ex;
  dx=F*x+G*(u+Jmp*w(i));
  dex=F*ex+G*u+L*Delt_x;
  x=x+dx*dt;
  exlast=ex;
  ex=ex+dex*dt;
  detx=F*ex+G*u; 
OutX=[OutX x];
OutEx=[OutEx ex];
i=i+1;
end


alpha0=0;alpha=[];
for i=1:length(OutX)
alpha0=alpha0+OutX(1,i)*dt;
alpha=[alpha rad2deg(alpha0)*60];
end



%=========================
%Решение уравнения Риккати
dt0=5e-5;
Sigp=[];
for t=0:dt0:0.5
   dSig=F*Sig+Sig*transpose(F)-Sig*transpose(C)*inv(Q)*C*Sig+G*W*transpose(G);  
   Sig=Sig+dSig*dt0;
   Sigp=[Sigp Sig(1,1)];
end
L=Sig*transpose(C)*inv(Q);

x=zeros(3,1);ex=zeros(3,1);
i=1;
OutX=[];OutEx=[];q=[];West=[];Sig_est=Sig;Wvar=W;
xlast=0;Sr=[];alx=0;Shum=[];Wful=[];F1=[];F2=[];
als=[];

for t=0:dt:T0
 u=-P*ex;
  y=C*x+1*0.1*pi/(180*60)*rand(1); 
  Delt_x=y-C*ex;
  dx=F*x+G*(u+1);
  dex=F*ex+G*u+L*Delt_x;
  x=x+dx*dt;
  exlast=ex;
  ex=ex+dex*dt;
  detx=F*ex+G*u; 
OutX=[OutX x];
end

alpha01=0;alpha1=[];

for i=1:length(OutX)
alpha01=alpha01+OutX(1,i)*dt;
alpha1=[alpha1 rad2deg(alpha01)*60];
end
