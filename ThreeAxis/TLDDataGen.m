function [out1, out2]= TLDDataGen(yt,k)
Q=[];z=[];  x=[];
for j=0:k
n=length(yt);
z=[zeros(1,j) yt(1:n-j)];
x=[x; z];
Q=[Q ;min(yt) max(yt)];
end
out1=x;
out2=Q;
end