function net=netFulLearn(xk,y,k1,n1,f1)
%k1- глубина запоминания
%n1- нейроннов  в скрытом слое
%f1 - количество выходов
[nn mm]=size(xk);
xi=xk;Q=[min(xi')' max(xi')'];
xk=xk;

for j=1:k1-1
    for l=1:nn
        x=xk(l,:);        
        n=length(x);
        z=[zeros(1,j) x(1:n-j)];
        xi=[xi; z];
        Q=[Q ;min(x) max(x)];
    end
end

f=[n1 10 f1];
net3=newff(Q,f,{'purelin' 'tansig' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs = 5000;
net3.trainParam.goal = 5e-11;
net3.trainParam.min_grad=1e-20;
net3=train(net3,xi,y);
net=net3
end