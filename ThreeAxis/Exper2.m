%%
%Исходные данные
H=0.12;

A0=0.016;
A3=7*A0
A2=4*A0;
A1=1*A0;
B=0.0005;
Kd=0.005;
h=0.03;
A=A0/5;
k=10;
%Матрица состояния для подсистемы "платформа"
FPL=[h/A1 0 0;
     0 h/A2 0;
     0  0  h/A3];
 
 %Матрица состояния для подсистемы "измерители"
 %Гиросокп №1 
FG1=[0  -H/B 0 0;   % Скоростные входы
     H/A   0 0 0;   %
     0     1 0 0;   % Выходы по углам
     1     0 0 0];

 %Гиросокп №1 
FG2=[0  -H/B 0 0;   % Скоростные входы
     H/A   0 0 0;   %
     0     1 0 0;   % Выходы по углам
     1     0 0 0];
 
 F0=zeros(3,4);
 F00=zeros(4,4);
 %Матрица состояния для подсистемы "измерения"

FPLG1=[1  0 0;
       0  1 0;
       0  0 0;       
       0  0 0];


FPLG2=[0  1 0;
       0  0 1;
       0  0 0;       
       0  0 0];
%Общая матрица сосотяния 
 
 
F=[FPL    F0 F0;
    FPLG1 FG1 F00';
    FPLG2 F00'  FG2];
% Отдельные каналы


G1=[1 0 0;
    0 1 0;
    0 0 1];

G2=[zeros(8,3)];


G=[G1;G2];

C=[zeros(5,3);
    [1 0 0;
     0 1 0];
    zeros(2,3);
    [0 1 0;
    0 0 1]]';
w0=10;w1=1;;w2=2;w3=3;w4=4;w5=5;   ksi=0.85;
goal=sort([desir(w1,ksi) desir(w2,ksi) desir(w3,ksi) desir(w4,ksi) desir(w5,ksi)  -1/w0]); 
%P=place(F,G,goal);
P=lqr(F,G,eye(11),eye(3),G)
% goal2=sort([desir(w1,ksi) -1/w0]); 
% Pext=place(F110,G,goal2);
% ExtSys=ss((F110-G*Pext),G,eye(3),[]);
% Pmed=place(F220,G,goal2);
% MedSys=ss((F220-G*Pmed),G,eye(3),[]);
% Pin=place(F330,G,goal2);
% InSys=ss((F330-G*Pin),G,eye(3),[]);
% 
 dt=0.005;
 %Моделирование в предположение что все наблюдаемо
 t=0:dt:4*pi;
 w=randn(3,length(t));
 w1=1*sin(0.5*2*pi*t)+0.00*randn(1,length(t));
 w2=[1*sin(0.5*2*pi*t);1*sin(0.5*2*pi*t+pi/2);1*sin(0.5*2*pi*t-pi/2)];    
 w3=[1*sin(0.5*2*pi*t);1*sin(2*pi*t+pi/2);1*sin(0.25*2*pi*t-pi/2)];    
 FullSys=ss((F-G*P),G,eye(11),[]);
% ForKalaman=ss(F1,G1,C1,[]);
 xf=lsim(FullSys,w,t);
% % xfIn=lsim(InSys,w1,t);
% % xfMed=lsim(MedSys,w1,t);
% % xfExt=lsim(ExtSys,w1,t);
% %xf=step(Mysys,t);
% Q=0.1*eye(3);
% W=0.1*eye(3);
% Psep=[Pext zeros(1,6);
%       zeros(1,3) Pmed zeros(1,3);
%       zeros(1,6) Pin];
% 



%%
%Синтез Калмана(линеного)
% 
% Q=0.1*eye(3);
% W=0.1*eye(3);
% 
% % % Sx=load('sig.mat');
% % % Sig=Sx.Sig;
% %  Sig=zeros(9);
% %  dt0=1e-5;
% %  Sigp=[];
% % %  
% %  for t=0:dt0:8
% %     dSig=F1*Sig+Sig*transpose(F1)-Sig*transpose(C1)*inv(Q)*C1*Sig+G1*W*transpose(G1);  
% %     Sig=Sig+dSig*dt0;
% %     Sigp=[Sigp Sig(3,3)];
% %  end
% % K=Sig*transpose(C1)*inv(Q);
% % 
% 
% [kest,K]=kalman(ForKalaman,Q,W);
% % w0=40;w1=30;w2=50;w3=35;w4=45;ksi=0.85;
% % goal=sort([desir(w1,ksi) desir(w2,ksi) desir(w3,ksi) desir(w4,ksi) -1/w0]); 
% % K=place(F1',C1',goal)';
% 
% damp(F1-K*C1);
% ex=zeros(9,1);OutX=[];
% x=zeros(9,1);
% alpha0=[0;0;0];
% alpha=[];
% 
% for t=0:1e-6:2
%  u=-P*ex;
%  y=C1*x; 
%  dx=F1*x+G1*u+G1*randn(3,1);
%  dex=(F1-K*C1)*ex+G1*u+K*y;
%  x=x+dx*dt;
%  ex=ex+dex*dt;
%  OutX=[OutX x];
%  alpha0=alpha0+[x(1);x(4);x(7)]*dt;
%  alpha=[alpha rad2deg(alpha0)*60];
% end

% 

%%
%С нейронными сетями
%
% 
% 
 x=C*xf';
 y=xf';
 net0=netFulLearn(x,y,k,8,3,5,11);
% 
%x=C1*xf';
%y=xf';
%x1=[x];
%us=P*xf()';
%net01=netSingLearn(x,y(1,:),k,12,1);
% % net02=netSingLearn(x,y(2,:),k,8,1);
% % net03=netSingLearn(x,y(3,:),k,8,1);
  %net04=netSingLearn(x,y(4,:),k,12,1);
% % net05=netSingLearn(x,y(5,:),k,8,1);
% % net06=netSingLearn(x,y(6,:),k,8,1);
%  net07=netSingLearn(x,y(7,:),k,12,1);
% % net08=netSingLearn(x,y(8,:),k,8,1);
% % net09=netSingLearn(x,y(9,:),k,8,1);

% 
% x=xfExt(:,3)';
% y=xfExt';
% net1=net3learn(x,y,k,6);
% 
% x=xfMed(:,3)';
% y=xfMed';
% net2=net3learn(x,y,k,6);
% 
% x=xfIn(:,3)';
% y=xfIn';
% net3=net3learn(x,y,k,6);


%%
%Мега моделирование
% 
% InNet1=zeros(k,1);
% InNet2=zeros(k,1);
% InNet3=zeros(k,1);
% xest1=zeros(3,1);
% xest2=zeros(3,1);
% xest3=zeros(3,1);
% 
% xest=[xest1;xest2;xest3];
% x=zeros(9,1);
% OutX=[];OutXest=[];
% k1=k;k2=k;k3=k;
% Psep=[Pext zeros(1,6);
%       zeros(1,3) Pmed zeros(1,3);
%       zeros(1,6) Pin];
% for t=0:dt:20
%   u=-P*xest;
%   tin1=InNet1; 
%   tin2=InNet2;
%   tin3=InNet3;
%   dx=F1*x+G1*u+G2;% G; G];
%   x=x+dx*dt;
%   y=C1*x; 
%   % Сеть канала №1
%   tin1=[y(1); tin1];    
%   InNet1=tin1(1:k1);    
%   xest1=sim(net1,[InNet1]);
%   % Сеть канала №2
%   tin2=[y(2); tin2];    
%   InNet2=tin2(1:k2);    
%   xest2=sim(net2,[InNet2]);
%   % Сеть канала №3
%   tin3=[y(3); tin3];    
%   InNet3=tin3(1:k3);    
%   xest3=sim(net3,[InNet3]);
%   xest=[xest1;xest2;xest3]; 
%   tin1=[];
%   tin2=[];
%   tin3=[];
%   OutX=[OutX x];
%   OutXest=[OutXest xest];
% end
% 
% 

%%
%Мега моделирование #2
% % 
k1=3*20;
k2=3*k;
InNet4=zeros(k1,1);

xest1=zeros(9,1);
InNet1=zeros(k2,1);
xest=[xest1];
x=zeros(9,1);
OutX=[];OutXest=[];
Psep=[Pext zeros(1,6);
    zeros(1,3) Pmed zeros(1,3);
    zeros(1,6) Pin];
alpha0=[0;0;0];
alpha=[];xest0=[];
for t=0:dt:20
    u=-P*x;
    tin1=InNet1;
  %  tin4=InNet4;
  % dx=F1*x+G1*u+G1*[1*sin(0.5*2*pi*t);1*sin(0.5*2*pi*t+pi/2);1*sin(0.5*2*pi*t-pi/2)];% G; G];
    dx=F1*x+G1*u+1*G1*[1;1;1];% G; G];
    x=x+dx*dt;
    x(3)=0.035*satlins(x(3)/0.035);x(6)=0.035*satlins(x(6)/0.035);
    x(9)=0.035*satlins(x(9)/0.035);
    y=C1*x;
    % Сеть канала №1
    tin1=[y; tin1];
    InNet1=tin1(1:k2);
%     tin4=[y; tin4];
%     InNet4=tin4(1:k1);

xest=sim(net0,[InNet1]);
%    xest1=sim(net01,[InNet1]);
%      xest2=sim(net02,[InNet4]);
%      xest3=sim(net03,[InNet4]);
%     xest4=sim(net04,[InNet4]);
%      xest5=sim(net05,[InNet4]);
%      xest6=sim(net06,[InNet4]);
%     xest7=sim(net07,[InNet1]);
%      xest8=sim(net08,[InNet4]);
%      xest9=sim(net09,[InNet4]);    
    xest0=[xest0 xest];
    tin1=[];
    tin4=[];
    %tin3=[];
    
    OutX=[OutX x];
    OutXest=[OutXest xest1];
    alpha0=alpha0+[x(1);x(4);x(7)]*dt;
    alpha=[alpha rad2deg(alpha0)*60];
end


