function [out]=sim_net(net,x_in,k1)
k1=k1+1;
InNet1=zeros(k1,1);
N = length(x_in);
x_out = [];
for i=1:N;
    tin1=InNet1;
    tin1=[x_in(i); tin1];
    InNet1=tin1(1:k1);
    x_out=[x_out sim(net,[InNet1])];
end
out=x_out;
end