function [result,result2] = nntraintool(command,varargin)
%NNTRAINTOOL Neural network training tool
%
%  Syntax
%
%    nntraintool
%    nntraintool('close')
%
%  Description
%
%    NNTRAINTOOL opens the training window GUI. This is launched
%    automatically when TRAIN is called.
%
%    To disable the training window set the following network training
%    property.
%
%      net.<a href="matlab:doc nnproperty.net_trainParam">trainParam</a>.<a href="matlab:doc nnparam.showWindow">showWindow</a> = false;
%
%    To enable command line training instead.
%
%      net.<a href="matlab:doc nnproperty.net_trainParam">trainParam</a>.<a href="matlab:doc nnparam.showCommandLine">showCommandLine</a> = true;
%
%    NNTRAINTOOL('close') closes the window.

% Copyright 2007-2010 The MathWorks, Inc.
result = false;
result2 = false;