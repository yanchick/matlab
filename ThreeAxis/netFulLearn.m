function net=netFulLearn(xk,y,k1,n1,m1,m2,z2)
xi=[];Q=[];
xk=xk;

for j=1:k1
    for l=1:m1
        x=xk(l,:);        
        n=length(x);
        z=[zeros(1,j) x(1:n-j)];
        xi=[xi; z];
        Q=[Q ;min(x) max(x)];
    end
end

f=[n1 z2];
net3=newff(Q,f,{'tansig' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs = 3500;
net3.trainParam.goal = 5e-9;
net3.trainParam.min_grad=1e-20;
net3=train(net3,xi,y);
net=net3


end