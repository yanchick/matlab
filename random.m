%randn('state',100)           % set the state of randn
%
%
%T = 1; N = 5000; dt = T/N;
%t = 0:dt:T-dt;
%signal = sin(2*pi*(0:dt:T-dt))+1.0;
%simple_random = randn(N,1)';
%dW = zeros(1,N);             % preallocate arrays ...
%Wienner= zeros(1,N);              % for efficiency
%
%dW(1) = sqrt(dt)*randn;      % first approximation outside the loop ...
%Wienner(1) = dW(1);                % since W(0) = 0 is not allowed
%for j = 2:N
%   dW(j) = sqrt(dt)*randn;   % general increment
%   Wienner(j) = Wienner(j-1) + dW(j); 
%end
%
%
%model1 = signal+simple_random;
%model2 = signal+Wienner;
%model3 = signal.* simple_random;
%model4 = signal.* Wienner;
%model5 = simple_random.*sqrt(signal );
%
%net1= NNCalc(3,5,model1,signal);
%net2= NNCalc(3,5,model2,signal);
%net3= NNCalc(3,5,model3,signal);
%net4= NNCalc(3,5,model4,signal);
%
%
%simple_random = randn(N,1)';
%dW = zeros(1,N);             % preallocate arrays ...
%Wienner= zeros(1,N);              % for efficiency
%
%dW(1) = sqrt(dt)*randn;      % first approximation outside the loop ...
%Wienner(1) = dW(1);                % since W(0) = 0 is not allowed
%for j = 2:N
%   dW(j) = sqrt(dt)*randn;   % general increment
%   Wienner(j) = Wienner(j-1) + dW(j); 
%end
%
%
%test_signal = cos(3*2*pi*(0:dt:T-dt))+1.0;
%
%test_model1 = test_signal+simple_random;
%test_model2 = test_signal+Wienner;
%test_model3 = test_signal.* simple_random;
%test_model4 = test_signal.* Wienner;
%
%cleared1_1 = sim_net(net1,model1,3);
%cleared1_2 = sim_net(net1,model2,3);
%cleared1_3 = sim_net(net1,model3,3);
%cleared1_4 = sim_net(net1,model4,3);
%1
%cleared2_1 = sim_net(net2,model1,3);
%cleared2_2 = sim_net(net2,model2,3);
%cleared2_3 = sim_net(net2,model3,3);
%cleared2_4 = sim_net(net2,model4,3);
%2
%cleared3_1 = sim_net(net3,model1,3);
%cleared3_2 = sim_net(net3,model2,3);
%cleared3_3 = sim_net(net3,model3,3);
%cleared3_4 = sim_net(net3,model4,3);
%3
%cleared4_1 = sim_net(net4,model1,3);
%cleared4_2 = sim_net(net4,model2,3);
%cleared4_3 = sim_net(net4,model3,3);
%cleared4_4 = sim_net(net4,model4,3);
%4

rms1_1= std(cleared1_1-test_signal);
rms1_2= std(cleared1_2-test_signal);
rms1_3= std(cleared1_3-test_signal);
rms1_4= std(cleared1_4-test_signal);

rms2_1= std(cleared2_1-test_signal);
rms2_2= std(cleared2_2-test_signal);
rms2_3= std(cleared2_3-test_signal);
rms2_4= std(cleared2_4-test_signal);

rms3_1= std(cleared3_1-test_signal);
rms3_2= std(cleared3_2-test_signal);
rms3_3= std(cleared3_3-test_signal);
rms3_4= std(cleared3_4-test_signal);

rms4_1= std(cleared4_1-test_signal);
rms4_2= std(cleared4_2-test_signal);
rms4_3= std(cleared4_3-test_signal);
rms4_4= std(cleared4_4-test_signal);

test_model1 = test_signal+simple_random;
test_model2 = test_signal+Wienner;
test_model3 = test_signal.* simple_random;
test_model4 = test_signal.* Wienner;


QW = [rms1_1 rms1_2 rms1_3 rms1_4;
rms2_1 rms2_2 rms2_3 rms2_4;
rms3_1 rms3_2 rms3_3 rms3_4;
rms4_1 rms4_2 rms4_3 rms4_4;];

