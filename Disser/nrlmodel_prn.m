function [OutXfk]=nrlmodel_prn(dOmega)
%=======================================================================
%Основная исследовательска прога
%Двуосник с 6 сетками
% function [OutXfk,OutXns, OutXlm, OutNbfk, OutNbns, OutNblm]=nrlmodel()
%======================================================================
tt=load('rd.mat');%��������� ����� �� �����
%%=0;
kV=16;
kAdc=16;
load('nets2.mat')
wnom=tt.w0/7.5;
tt.w0=tt.w0/7.5;
A = [              0                       0                     0                   0                         1                            0          -1                          0;  
                    0                       0                     0                   0                         0                            1           0                         -1;                 
                    1                       0                     0                   0                         0                            0           0                          0;  
                    0                       1                     0                   0                         0                            0           0                          0;                 
                    0                   tt.kd*tt.w0                 0                   0                    -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)                   0   tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
              tt.kd*tt.w0                     0                     0                   0       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b)     -tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b)     0;      
                    0                       0                     0                   0                        0                             0     -tt.h0/tt.A0                        0; 
                    0                       0                     0                   0                        0                             0          0                       -tt.h0/tt.B0  ];
niz=2;
np=8;
B=[zeros(np-niz,2);eye(2)];
C=[eye(2) zeros(2,np-niz)];
C2=[eye(4) zeros(4,np-4)];

w0=20;w2=30;w3=24;w1=27;ksi=0.9;
goal1=sort( [desir(w0,ksi) desir(w1,ksi) desir(w2,ksi) desir(w3,ksi)]);
P=place(A,B,goal1);

w0=35;w2=40;w3=44;w1=47;ksi=0.9;
goal2=sort( [desir(w0,ksi) desir(w1,ksi) desir(w2,ksi) desir(w3,ksi)]);
L=place(A',C2',goal2).';

Ma=0.4;%��� �������
Umax=0.001*pi/(180*60);%��� ����
Q=[Umax 0;0 Umax];
W=[Ma 0;0 Ma];

Sig=zeros(np);

dt0=1e-5;
Sigp=[];

for t=0:dt0:0.5
   dSig=A*Sig+Sig*transpose(A)-Sig*transpose(C)*inv(Q)*C*Sig+B*W*transpose(B);  
   Sig=Sig+dSig*dt0;
end
K=Sig*transpose(C)*inv(Q);


tt.w0=tt.w0*(1+dOmega/100);
A = [              0                       0                     0                   0                         1                            0          -1                          0;  
                    0                       0                     0                   0                         0                            1           0                         -1;                 
                    1                       0                     0                   0                         0                            0           0                          0;  
                    0                       1                     0                   0                         0                            0           0                          0;                 
                    0                   tt.kd*tt.w0                 0                   0                    -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)                   0   tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
              tt.kd*tt.w0                     0                     0                   0       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b)     -tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b)     0;      
                    0                       0                     0                   0                        0                             0     -tt.h0/tt.A0                        0; 
                    0                       0                     0                   0                        0                             0          0                       -tt.h0/tt.B0  ];


                

k1_1=21;%Количество слоев задержки по состоянию
k1_2=2;%Количество слоев задержки по упраавлению


x_ns=zeros(np,1);ex_ns=zeros(np,1);xest_ns=zeros(np,1);i=1;
OutX_ns=[];u1_ns=[];Out11_ns=[];xestv_ns=[];u_ns=zeros(2,1);
Powns=[];

x_fk=zeros(np,1);ex_fk=zeros(np,1);xest_fk=zeros(np,1);i=1;
OutX_fk=[];u1_fk=[];Out11_fk=[];xestv_fk=[];u_fk=zeros(2,1);
Powfk=[];

x_lm=zeros(np,1);ex_lm=zeros(np,1);xest_lm=zeros(np,1);i=1;
OutX_lm=[];u1_lm=[];Out11_lm=[];xestv_lm=[];u_lm=zeros(2,1);
Powlm=[];


InputY1=zeros(1,k1_1);
InputY2=zeros(1,k1_1);

dt=5e-5; Tend=0.5
w=[0;0];
kv=kV;
net10=nnsetka(net1,kv);
net20=nnsetka(net2,kv);
net50=nnsetka(net5,kv);
net60=nnsetka(net6,kv);
net70=nnsetka(net7,kv);
net80=nnsetka(net8_1,kv);
y80=[];
f=6;
i=1;

for t=0:dt:(Tend-2*dt) 
 tt.w0=tt.w0*(1-exp(-t*f));
A = [              0                       0                     0                   0                         1                            0          -1                          0;  
                    0                       0                     0                   0                         0                            1           0                         -1;                 
                    1                       0                     0                   0                         0                            0           0                          0;  
                    0                       1                     0                   0                         0                            0           0                          0;                 
                    0                   tt.kd*tt.w0                 0                   0                    -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)                   0   tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
              tt.kd*tt.w0                     0                     0                   0       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b)     -tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b)     0;      
                    0                       0                     0                   0                        0                             0     -tt.h0/tt.A0                        0; 
                    0                       0                     0                   0                        0                             0          0                       -tt.h0/tt.B0  ];
   
    
   w=[rand(1); rand(1)];
   wiz=1e-4*[rand(1); rand(1)];

%================  
%ГС на базе НС
%================
  u_ns=-P*xest_ns*(1-exp(-t*f));%Управление
%   u1_ns=[u1_ns u_ns];
  dx_ns=A*x_ns+B*(u_ns+w);%Основная модель 
  x_ns=x_ns+dx_ns*dt;%Перменная состояния объекта
  y_ns=kvant(C*x_ns,kAdc)+wiz;%Наблюдение  
   
  InputY1=[y_ns(1) InputY1];
  InputY1=InputY1(1:(k1_1));
  InputY2=[y_ns(2) InputY2];
  InputY2=InputY2(1:(k1_1));
  Input=[InputY1' ; InputY2'];  
  y1=sim(net10,Input);
  y2=sim(net20,Input);
% y3=sim(net30,Input);
% y4=sim(net40,Input);
  y5=sim(net50,Input);
  y6=sim(net60,Input);
  y7=sim(net70,Input);
  y8=sim(net80,Input); 
  xest_ns= [y1 y2 x_ns(3:4)' y5 y6 y7 y8 ]';
  Out11_ns=[Out11_ns xest_ns];
  %Определение праметров наблюдения
  OutX_ns=[OutX_ns x_ns];
Powns=[Powns u_ns.*x_ns(7:8)];
%==============
% ГС на базе ФК
%==============
  u_fk=-P*ex_fk*(1-exp(-t*f));%Управление
% u1_fk=[u1_fk u_fk];
  y_fk=kvant(C*x_fk,kAdc)+wiz;%Наблюдение  
  Delt_x_fk=y_fk-C*ex_fk;%Разность между выходом фильтра и измерен
  dx_fk=A*x_fk+B*(u_fk+w);%Основная модель 
  dex_fk=kvant(A,kv)*ex_fk+kvant(B,kv)*u_fk+kvant(K,kv)*Delt_x_fk;%Фильтр
  x_fk=x_fk+dx_fk*dt;%Перменная состояния объекта
  exlast_fk=ex_fk;
  ex_fk=ex_fk+dex_fk*dt;%Переменная состояния фильтра  
  Out11_fk=[Out11_fk ex_fk]; 
  %Определение праметров наблюдения
  OutX_fk=[OutX_fk x_fk];
  Powfk=[Powfk u_fk.*x_fk(7:8)];
%==============
% ГС на базе ФЛ
%==============
  u_lm=-P*ex_lm*(1-exp(-t*f));%Управление
% u1_lm=[u1_lm u_lm];
  y_lm=kvant(C2*x_lm,kAdc)+[wiz;wiz];%Наблюдение  
% Delt_x_lm=y_lm-C*ex_lm;%Разность между выходом фильтра и измерен
  dx_lm=A*x_lm+B*(u_lm+w);%Основная модель 
  dex_lm=(kvant(A,kv)-kvant(L,kv)*kvant(C2,kv))*ex_lm+kvant(L,kv)*y_lm+kvant(B,kv)*u_lm;%Фильтр
  x_lm=x_lm+dx_lm*dt;%Перменная состояния объекта
  exlast_lm=ex_lm;
   ex_lm=ex_lm+dex_lm*dt;%Переменная состояния фильтра  
   Out11_lm=[Out11_lm ex_lm]; 
   %Определение праметров наблюдения
   OutX_lm=[OutX_lm x_lm]; 
   Powlm=[Powlm u_lm.*x_lm(7:8)];
   i=i+1
end
 OutXfk=[OutX_fk;
        Out11_fk; 
        OutX_ns; 
        Out11_ns;
        OutX_lm;
        Out11_lm;
        Powfk;
        Powns;
        Powlm];
%  OutXfk=[OutX_ns; 
%         Out11_ns;];
% yz8=y80;
end