function ntoolbx3dem
%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Моделирование, две сети
%======================================================================
%{
dig=16;%Разрядность
jd=2^dig;
net30=net3;


net3=net30;

net3.IW{1,1}=round(net30.IW{1,1}.*jd)./jd;
net3.LW{2,1}=round(net30.LW{2,1}.*jd)./jd;
net3.b{1}=round(net30.b{1}.*jd)./jd;
net3.b{2}=round(net30.b{2}.*jd)./jd;
%}

dt=0.01;
Tend=10;
x0=zeros(k3,1);
x=x0;
xz=x;
sqerr=[];sqerr2=[];
InNet1=zeros(k1_1,1);
InNet2=zeros(k2_1,1);
alpha=[0];
alphapr=0;
t1=0:dt:Tend;
u0=0;
xest=x0;
xwcap=[0;0;0];
Sqerr=[0;0;0];
  
for t=0:dt:Tend;
    w=rand(1);
    tin1=InNet1;
    tin2=InNet2;
    
    u=-P*xest;
    dx=F*x+G*u+G*w;   
    x=x+dx*dt;
    xz=[xz x];
    y=x(3)+0.01*rand(1);
    
    tin1=[y; tin1];
    tin2=[u; tin2];
    
    InNet1=tin1(1:k1_1);
    InNet2=tin2(1:k2_1);
    
    tin1=[];
    tin2=[];
    xest=sim(net3_1,[InNet1;InNet2]);
    Sqerr=[Sqerr sqrt((xest-x).^2)];

    sqerr=[sqerr xest];   
    alphapr=alphapr+x(1)*dt;
    alpha=[alpha; rad2deg(alphapr)*60];
end
end

function ntoolbx3dem
%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Расчет, одна сеть
%======================================================================
clear;
t=0:0.01:20*pi;
%w=0.1*randn(length(t),1)';
w=sin(2*pi*0.5*t);
ksi=0.7;
w0=1*2*pi;
H=0.12;
A=0.016;
B=0.0005;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];
G=[1; 0 ;0];
C=eye(3);  
D=zeros(3,1);
w0=1;w1=3;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
Mysys=ss((F-G*P),G,C,D);
xf=lsim(Mysys,w,t);
k1_1=6;%Количество слоев задержки по состоянию
k2_1=0;%Количество слоев задержки по упраавлению
k3=3;%Порядок системы
x=[];
Q=[];

%=============================
%Первая подсистема - гироблок
%=============================
y_target=xf;    
y=xf(:,3)'+0.00*max(xf(:,3)')*rand(1,length(xf(:,3)'));

for j=1:k1_1
n=length(y);
z=[zeros(1,j) y(1:n-j)];
x=[x; z];
Q=[Q ;min(y) max(y)];
end

%plot(t,sin(t),t,x);

net3_1=newff(Q,[5 3],{'purelin' 'purelin'},'trainlm');
net3_1.trainParam.show = 2;
net3_1.trainParam.epochs = 5000;
net3_1.trainParam.goal = 1e-7;
net3_1.trainParam.min_grad=1e-20;
net3_1=train(net3_1,x,y_target');
end
