function [f] = LapTanh(z)
f = 0.5*cpsi(z./4+0.5)-0.5*cpsi(z./4)-1./z;
return