clear all;
dt=5e-4;%Шаг интегрирования
T0=5;%Время интегрирования
Nstep=1000;%Объем памяти
Jk=3000;%Точка переключения
T=0:dt:T0;
N=length(T);
H=0.12;
A=0.016;
B=8e-5;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];%Матрица состояния
G=[1; 0 ;0]; %Матрица Управления
C=[0 0 1];  %Матрица наблюдения
w0=5;w1=10;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
%Формирования случайных величин
Ma=0.4;%СКО момента
Umax=1*0.1*pi/(180*60);%СКО шума
%Измерительный Шум
v1=Umax*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Umax/st);
v=v1;
clear v1;
%Форимирование случайного воздействия по оси ГС
v1=Ma*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Ma/st);
w=v1;
clear v1 v2;
%Формирования корреляционых матриц
Q=Umax^2; 
W=Ma^2;
Sig=zeros(3);
%Решение уравнения Риккати
dt0=0.001;
Sigp=[];
for t=0:dt0:0.3
   dSig=F*Sig+Sig*transpose(F)-Sig*transpose(C)*inv(Q)*C*Sig+G*W*transpose(G);  
   Sig=Sig+dSig*dt0;
   Sigp=[Sigp Sig(1,1)];
end
K=Sig*transpose(C)*inv(Q);
%Моделирование Системы
x=zeros(3,1);ex=zeros(3,1);
i=1;
OutX=[];OutEx=[];q=[];West=[];Sig_est=Sig;Wvar=W;
Jmp=5;xlast=0;Sr=[];alx=0;Shum=[];Wful=[];F1=[];F2=[];
u1=[];
als=[];
for t=0:dt:T0
 u=-P*ex;%Управление
 u1=[u1 u];
  y=C*x+v(i);%Наблюдение  
  Delt_x=y-C*ex;%Разность между выходом фильтра и измерен
  dx=F*x+G*(u+Jmp*w(i));%Основная модель 
  dex=F*ex+G*u+K*Delt_x;%Фильтр
  x=x+dx*dt;%Перменная состояния объекта
  exlast=ex;
  ex=ex+dex*dt;%Переменная состояния фильтра
  %Эталонная модель
  detx=F*ex+G*u;%Основная модель  
  %Определение праметров наблюдения
OutX=[OutX x];
i=i+1;
end
