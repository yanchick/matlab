%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Моделирование, две сеть
%======================================================================
%{
dig=16;%Разрядность
jd=2^dig;
net4=net40;
net3=net30;
net4=net40;

net4.IW{1,1}=round(net40.IW{1,1}.*jd)./jd;
net4.LW{2,1}=round(net40.LW{2,1}.*jd)./jd;
net4.b{1}=round(net40.b{1}.*jd)./jd;
net4.b{2}=round(net40.b{2}.*jd)./jd;

net3=net3;

net3.IW{1,1}=round(net30.IW{1,1}.*jd)./jd;
net3.LW{2,1}=round(net30.LW{2,1}.*jd)./jd;
net3.b{1}=round(net30.b{1}.*jd)./jd;
net3.b{2}=round(net30.b{2}.*jd)./jd;
%}

dt=0.01;
Tend=10;
x0=zeros(k3,1);
x=x0;
xz=x;
sqerr=[];sqerr2=[];
InNet11=zeros(k1_1,1);%первая сетка
InNet12=zeros(k2_1,1);
InNet211=zeros(k1_2,1);
InNet212=zeros(k1_2,1);
InNet22=zeros(k2_2,1);
t1=0:dt:Tend;
u=0;
xzer=[];
 xwcap=[0;0;0];
 xest=xwcap;
 xwa=[];
  Sqerr=[];
  alphapr=0;
  alpha=[];
for t=0:dt:Tend;
    
    w=1;   
    tin11=InNet11;
    tin12=InNet12;    
    tin211=InNet211;
    tin212=InNet212; 
    tin22=InNet22;
    
    u=-P*xwcap;
    dx=F*x+G*u+G*w;   
    x=x+dx*dt;
    xz=[xz x];
    y=x(3)+0.00*rand(1);
 %Первая сетки   
    tin11=[y; tin11];
     tin12=[u; tin12];    
      InNet11=tin11(1:k1_1);
       InNet12=tin12(1:k2_1);    
        InNet110=[InNet11;InNet12];    
         xest=sim(net3_2_1,InNet110);
          xest1=xest(1);    
        xest2=xest(2);
        
    sqerr=[sqerr xest];   
   %Вторая сетки 
    tin211=[xest1; tin211];
    tin212=[xest2; tin212];
    tin22=[u; tin22];    
    InNet211=tin211(1:k1_2);
    InNet212=tin212(1:k1_2);    
    InNet22=tin22(1:k2_2);    
    
    InNet220=[InNet211; InNet212 ;InNet22];    
    tin211=[];
    tin212=[];   
    tin22=[];     
    xest22=sim(net3_2_2,InNet220);    
    xzer=[xzer x];
    xwcap=[xest22];
    
    xwa=[xwa xwcap];
    Sqerr=[Sqerr sqrt((xwcap-x).^2)];
    alphapr=alphapr+x(1)*dt;
    alpha=[alpha; rad2deg(alphapr)*60];
end

