function y=kvant(x,dig)
alphamax=deg2rad(10/60);
kv=alphamax/(2^dig);
y=round(x./kv).*kv;