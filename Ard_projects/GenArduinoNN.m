function GenArduinoNN(net,file)
W{1}=net.IW{1};
b=net.b;
[nn mm]=size(net.layers)
for i=1:nn
    tf{i}=net.layers{i}.transferFcn;
     if (strcmp(net.layers{i}.transferFcn,'tanh')) 
         tf{i}='tanh'; 
     elseif (strcmp(net.layers{i}.transferFcn,'')) 
         tf{i}=''; 
     end
end

for i=2:nn
    W{i}=net.LW{i,i-1}
       
end
fi=fopen(file,'w');

for k=1:nn    
    [n m]=size(W{k});
    if (k==1) in='x'; out=strcat('y',num2str(k)); end
    if (k~=1)  in=strcat('y',num2str(k-1)); out=strcat('y',num2str(k)); end
      for i=1:n
        fprintf(fi,'%s[%d] = %s(',out,i-1,tf{k});        
        for j=1:m
            if (j~=m)
                fprintf(fi,'%f*%s[%d]+',W{k}(i,j),in,j-1);
            end
            if (j==m)
               fprintf(fi,'%f*%s[%d] + %f );\n',W{k}(i,j),in,j-1, b{k}(i) );
            end
        end
    end
end
fclose(fi);
end
