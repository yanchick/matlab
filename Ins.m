clear
G=6.67259e-11;
M=5.97e24;  
R=6378000;
g=9.81;
V1=sqrt(G*M/R);
L=9000e3;
dL=800/sqrt(2);%Ошибка на 1 ЧЭ
Fi=(L/R);
teta=0:0.01:1;
V=V1*sqrt(tan(Fi/2)./(sin(teta).*cos(teta)+cos(teta).*cos(teta)));
%{
figure;
plot(rad2deg(teta),V);
grid on;
xlabel('teta, grads');
ylabel('V, m/sec');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print ('-djpeg','-r300','skor.jpg');
%}
h=0.5*R*(sin(Fi/2)+cos(Fi/2)-1); % Высота траектории
[Voptim i]=min(V);%ОTптимальная скорость
AngOptim=teta(i);%Оптимальный угол

DlDv=2*R*(sin(Fi)+cot(AngOptim)*(1-cos(Fi)))/Voptim;%Коэффициент ошибки по скорости
dl=dL/DlDv;%Погрешность акселерометра в м/с

DlDte=2*R*(sin(Fi+2*AngOptim)/sin(2*AngOptim)-1);%Коэффициент ошибки по углу

dte=60*rad2deg(dL/DlDte);
DlDh=2*cot(AngOptim)-cos(Fi+AngOptim)/sin(AngOptim);%Коэффициент ошибки по высоте

%Закон изменеия ускорения

Tnach=0;Vnach=0;
Tkonech=60;Vkonech=Voptim;
t0=Tnach:0.1:Tkonech;

T=[Tkonech^2 Tkonech;
   Tkonech^3/3 Tkonech^2/2];

Y0=[0 ; Voptim];

[ab]=T\Y0;a=ab(1);b=ab(2);
%a=3*Voptim/Tkonech^3;
%accel=a*t0.*t0;
accelFun=@(t) a*t.*t+b*t;%Сам закон
acc=accelFun(t0);
%{
figure;
plot(t0,acc)
grid on;
xlabel('t, seconds');
ylabel('a, m/sec^2');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print ('-djpeg','-r300','accel.jpg');
%}

Beta0 = 0.53;
BetaH = 1;%1.041;
BetaT =-0.0056;%1/(2*Tkonech);
betaFun=@(t) Beta0+BetaH*exp(BetaT*t);
bee=betaFun(t0);

%{
figure;
plot(t0,rad2deg(bee))
grid on;
xlabel('t, seconds');
ylabel('beta, grad');
title(' ');
set(gca,'GridLineStyle','-','LineWidth',1);
print ('-djpeg','-r300','b.jpg');
%}



acSin=@(t)accelFun(t).*sin(betaFun(t)).*sin(betaFun(t));
acCos=@(t)accelFun(t).*cos(betaFun(t)).*cos(betaFun(t));

intSin2=0;intCos2=0;
int2Sin2=0;int2Cos2=0;
intSin=0;intCos=0;
int2Sin=0;int2Cos=0;
intSin2t=0;intCos2t=0;
int2Sin2t=0;int2Cos2t=0;
dtt=0.1;

for t=0:dtt:Tkonech
    
intSin2=intSin2+dtt*(accelFun(t).*accelFun(t).*sin(betaFun(t)).*sin(betaFun(t)));
intCos2=intCos2+dtt*(accelFun(t).*accelFun(t).*cos(betaFun(t)).*cos(betaFun(t)));

int2Sin2=int2Sin2+dtt*intSin2;
int2Cos2=int2Sin2+dtt*intCos2;

intSin=intSin+dtt*(accelFun(t).*sin(betaFun(t)));
intCos=intSin+dtt*(accelFun(t).*cos(betaFun(t)));
int2Sin=int2Sin+dtt*(intSin);
int2Cos=int2Sin+dtt*(intCos);

intSin2t=intSin2t+dtt*(accelFun(t).*t.*sin(betaFun(t)).*sin(betaFun(t)));
intCos2t=intCos2t+dtt*(accelFun(t).*t.*cos(betaFun(t)).*cos(betaFun(t)));
int2Sin2t=int2Sin2t+dtt*intSin2;
int2Cos2t=int2Sin2t+dtt*intCos2;

end

%==============================
%===Выработка требований к ЧЭ==
%==============================
%Гироскопы
%sqrt(2*200^2+300^2+100^2)

alpha0_x=asin(300/(g*(Tkonech^2/2+DlDv*Tkonech)));
alpha0_y=acos(1-100/(g*DlDh*(Tkonech^2/2+DlDv*Tkonech)));

wdrx=(100/(int2Cos2t))*2.0626e5;
wdry=(100/(int2Sin2t))*2.0626e5;

%Акселерометры
dL=692;
dAks=dL/sqrt(6);
La0=15;
a0_x=La0/(Tkonech^2/2+DlDv*Tkonech);
a0_y=La0/(DlDh*Tkonech^2/2+DlDh*DlDv*Tkonech);
%
La1=210;
a1_x=La1/(int2Cos+DlDv*intCos);
a1_y=La1/(DlDh*(int2Sin+DlDv*intSin));
%
La2=460;
a2_x=La2/(int2Cos2+DlDv*intCos2);
a2_y=La2/(DlDh*(int2Sin2+DlDv*intSin2));

a0 = min(a0_x,a0_y) 
a1 = min(a1_x,a1_y)
a2 = min(a2_x,a2_y)
alpha0 = rad2deg(min(alpha0_x,alpha0_y))*60*60
wdr = min(wdrx,wdry)
%}