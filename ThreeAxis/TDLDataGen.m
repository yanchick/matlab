function [xi,Qi]=TDLDataGen(xk,k1)
%k1- глубина запоминания
[nn mm]=size(xk);
xi=[];Q=[];
xk=xk;

for j=0:k1
    for l=1:nn
        x=xk(l,:);        
        n=length(x);
        z=[zeros(1,j) x(1:n-j)];
        xi=[xi; z];
        Q=[Q ;min(x) max(x)];
    end
end
[xi]=[xi];
Qi=Q;
end