% %=======================================================================
% %Объект первого порядка с нейронным регулятором
%=======================================================================
clear all
t=0:0.1:4*pi;
%w=0.7*randn(length(t),1);
w=sin(2*pi*1.5*t);
%w=0*t;
ksi=0.95;
w0=4*2*pi;
T=1;
k=1;
A=[0 1; -1/(T^2) -2*ksi/(T)];
B=[0; 1];
B0=[0; k];
C=[1 0;
    0 1];
D=[];


T0=1.3; ksi0=0.92;
A0=[0 1;  -1/(T0^2)  -2*ksi0/(T0) ];
P=B0'*(A-A0);
P=[((1/T^2)-(1/T0^2)) ((2*ksi/T)-(2*ksi0/T0))];
Mysys=ss(tf({[1 0]; [1]},{[T^2 2*ksi*T 1];[T^2 2*ksi*T 1]}));
Mysys0=ss(tf({[1 0]; [1]},{[T0^2 2*ksi*T0 1];[T0^2 2*ksi*T0 1]}));
%xf=step(Mysys,t); xf=lsim(Mysys,w,t,[0; 90]);
xf=lsim(Mysys,w,t);
F=A;G=B;
[A,B,C,D]=ssdata(Mysys);
[A0,B0,C0,D0]=ssdata(Mysys0);
P=B\(A-A0)

y=xf(:,1)';
k1=3;%Количество слоев задержки по состоянию
k2=0;
x=[];
Q=[];
% 

for j=1:k1
n=length(y);
z=[zeros(1,j) y(1:n-j)];
x=[x; z];
Q=[Q ;-5.0 5.0];
end

y_target=xf;    
%plot(t,sin(t),t,x);


net3=newff(Q,[5 2],{'tansig' 'purelin'},'trainlm');

net3.trainParam.show = 2;
net3.trainParam.epochs = 5000;
net3.trainParam.goal = 1e-7;
net3.trainParam.min_grad=1e-100;
net3.trainParam.mu_max=1e100;
[net3 p]=train(net3,x,y_target');

dt=0.01;
Tend=15.5;
t=0:dt:Tend;
N=length(t);
T0=Tend;
x0=zeros(2,1);xest=x0;
x=x0;
x=[90;0];
xz=x;xzz=x;
sqerr=[];
InNet1=zeros(k1,1);
InNet2=zeros(k2,1);
t1=0:dt:Tend;
u=[0];
x=[90;0];
for t=0:dt:Tend;
  % w=rand(1);   
    tin1=InNet1;
    tin2=InNet2;
    u=P*xest;   
%     if (t==10)
%         x(1)=30;
%     end
    y=x(1);
    tin1=[y; tin1];
    tin2=[u; tin2];
    dx=A*x-B*u;   
    x=x+dx*dt;
    xz=[xz x];
    xzz=[xzz xest];

    InNet1=tin1(1:k1);
    InNet2=tin2(1:k2);
    tin1=[];
    tin2=[];
   xest=sim(net3,[InNet1;InNet2]);
end
    
% % %     sqerr=[sqerr xest];   
% % %     alphapr=alphapr+x(1)*dt;
% % %     alpha=[alpha; rad2deg(alphapr)*60];