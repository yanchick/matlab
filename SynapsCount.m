function [Nmin,Nmax] =  SynapsCount(N,Nin,Nout)
Nmin=round(N*Nout/(1+log2(N)));
Nmax=round(Nout*(N/Nin+1)*(Nin+Nout+1)+Nout);