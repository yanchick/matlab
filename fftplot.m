function [Freq_max]=fftplot(y,Fs)
L=length(y);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;
Freq = Fs/2*linspace(0,1,NFFT/2+1);
Pow=2*abs(Y(1:NFFT/2+1));
[Pmax, i]=max(Pow);
Pow_max=Pmax;
Freq_max=1.0*i*Freq(2);
% Plot single-sided amplitude spectrum.
plot(Freq,Pow) 
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')
end