function R = myautocorr(x,t)
Rtemp=zeros(t+1,1);
xm=mean(x);
for j=0:t 
for i=1:length(x)-(t+10)
    Rtemp(j+1) = Rtemp(j+1) + (x(i)-xm)*(x(i+j)-xm);
    end
end;
%R=xcorr(x,'biased');
R=Rtemp/length(x);
end