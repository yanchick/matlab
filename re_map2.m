function out= re_map2(x,in_min,in_max,out_min, out_max)
out=re_map(floor(re_map(x,in_min,in_max,out_min,out_max)),out_min,out_max,in_min,in_max);
 end
