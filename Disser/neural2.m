
%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Моделирование, две сети
%======================================================================
t=load('rd.mat');%��������� ����� �� �����
global A8 C8out B8 Q W G1 P;
A = [              0                       0                     0                   0                         1                            0          -1                          0;
    0                       0                     0                   0                         0                            1           0                         -1;
    1                       0                     0                   0                         0                            0           0                          0;
    0                       1                     0                   0                         0                            0           0                          0;
    0                   t.kd*t.w0*0                 0                   0                    -t.kd/(t.B_+t.b)  (t.A_+2*t.b)*t.w0/(t.B_+t.b)                   0   t.w0*(t.A_+t.a+t.b-t.c)/(t.B_+t.b);
    t.kd*t.w0*0                     0                     0                   0       -(t.A_+2*t.b)*t.w0/(t.B_+t.b)             -t.kd/(t.B_+t.b)     -t.w0*(t.A_+t.a+t.b-t.c)/(t.B_+t.b)     0;
    0                       0                     0                   0                        0                             0     -t.h0/t.A0                        0;
    0                       0                     0                   0                        0                             0          0                       -t.h0/t.B0  ];
A(:,3:4)=[];
A(3:4,:)=[];
niz=2;
dt=0.0005;
Tend=pi;
t=0:dt:Tend;
%w=[0.1*randn(length(t),1)'; 0.1*randn(length(t),1)'];
%w=[sin(0.2*t.*t); sin(0.2*t.*t)];
B=[zeros(4,2);eye(2)];
C=[eye(6)] ;
D=[];
D=zeros(2,6);
% Gs=ss(A,B,C,0);%������� ��� Simulink
C=[eye(niz) zeros(niz,6-niz)] ;
w0=4.5;w2=4;w3=5;ksi=0.9;
goal1=sort([desir(w0,ksi) desir(w2,ksi) desir(w3,ksi)]);
P=place(A,B,goal1);

dt=0.0005;
k1_1=12;
Tend=2*pi;
x0=zeros(6,1);
xk=x0;
x0(1)=0.00001;
x=x0;
xz=x;
sqerr=[];sqerr2=[];
alpha=[0];
alphapr=0;
u0=0;
xest=zeros(6,1);
xwcap=[0;0;0];
Sqerr=[0;0;0];
tin1=[]; tin2=[];
InNet1=zeros(k1_1,1);%первая сетка
InNet2=zeros(k1_1,1);

for t=0:dt:Tend;
    w=[0 0]';
    
    tin1=InNet1;
    tin2=InNet2;    
     
    u=-P*x;
    %Объект
    dx=A*x+B*u+B*w;   
    x=x+dx*dt;
  
   xz=[xz x];
    y1=x(1);
    y2=x(2);
     tin1=[ y1; tin1];
         tin2=[y2; tin2 ];    
     
      InNet1=tin1(1:k1_1);
       InNet2=tin2(1:k1_1);    
       
    tin1=[];
    tin2=[];
   
    xest246=sim(net33,[InNet1; InNet2])
  % xest135=sim(net3_2,[InNet1; InNet2]);
    
    xest=[xest; xest246];
    
    alphapr=alphapr+x(6)*dt;
    alphapr
    alpha=[alpha; rad2deg(alphapr)*60];
end

max(alpha)
sum(DeelX')./length(DeelX);