function [disper ClSign NSign]=ModSchem(Stabil,T,P,G,v,w)
%������������� �� ��������
G1=G;
[A B C D]=ssdata(Stabil);
sx=[];
step=0.1;
for sl=2:1:100    
ds=step*sl;    
G=ds*G1;
%������ ������� ����������� ��� ����� ����
Dzer=zeros(2,2);
Deye=eye(2);
D=[Dzer Deye;Dzer Dzer];
Gs8=ss(A,[B 0*B],[C;C],D,'inputname',{'Mom1','Mom2','Noise1','Noise2'},'outputname',{'Alpha_Noisy','Beta_Noisy','tAlpha','tBeta'});%������� ��� Simulink
%������ �������
Aest=[A-G*C-B*P];
Best=G;
Cest=[zeros(2,8); eye(8)];
Dest=[Deye; zeros(8,2)];
Kalman=ss(Aest,Best,Cest,Dest,'inputname',{'A1','B2'},'outputname',{'mesAlpha','mesBeta','Alpha','Beta','intAlpha','intBeta','pAlpha','pBeta','plAlpha','plBeta'});
%�����������
sys=series(Gs8,Kalman,[1 2],[1 2]);
SimModel=feedback(sys,P,[1 2],[3 4 5 6 7 8 9 10]);
SimModel=SimModel([1 2 3 4],[1 2 3 4]);
[out,x]=lsim(SimModel,[w' v'],T);
ClearSignal=(out(:,1)-v(1,:)');
sx=[sx [rad2deg(std(out(:,3)-ClearSignal))*60*60*10;ds]];
end
[disper ClSign NSign]=[sx rad2deg(ClearSignal)*60 rad2deg(out(:,3))*60];
end