%Фльтр Калмана
%==================================================== 
function Out=kalman12()
t=load('rd.mat');%��������� ����� �� �����
global A8 C8out B8 Q W G1 P;
A = [              0                       0                     0                   0                         1                            0          -1                          0;  
                    0                       0                     0                   0                         0                            1           0                         -1;                 
                    1                       0                     0                   0                         0                            0           0                          0;  
                    0                       1                     0                   0                         0                            0           0                          0;                 
                    0                   t.kd*t.w0                 0                   0                    -t.kd/(t.B_+t.b)  (t.A_+2*t.b)*t.w0/(t.B_+t.b)                   0   t.w0*(t.A_+t.a+t.b-t.c)/(t.B_+t.b);
              t.kd*t.w0                     0                     0                   0       -(t.A_+2*t.b)*t.w0/(t.B_+t.b)             -t.kd/(t.B_+t.b)     -t.w0*(t.A_+t.a+t.b-t.c)/(t.B_+t.b)     0;      
                    0                       0                     0                   0                        0                             0     -t.h0/t.A0                        0; 
                    0                       0                     0                   0                        0                             0          0                       -t.h0/t.B0  ];
A(:,3:4)=[];
A(3:4,:)=[];
niz=2;
B=[zeros(4,2);eye(2)];
%C8out1=[eye(4) zeros(4,4)];
C=[eye(niz) zeros(niz,6-niz)] ;
D=[zeros(2,2) eye(2)];
%C=eye(6);
Do=zeros(2,6);
%C8int=[zeros(2,6) eye(2)];
Gs=ss(A,B,C,0,'outputnames',{'Alpha','Beta'},'inputnames',{'Mom1','Mom2'});%������� ��� Simulink
w0=5;w2=20;w3=25;ksi=0.9;
goal1=sort([desir(w0,ksi) desir(w2,ksi) desir(w3,ksi)]);
P=place(A,B,goal1);

%Решение уравнения Риккати
%========================================
Ma=0.4;%��� �������
N=15000;
Umax=1*0.1*pi/(180*60);%��� ����

v1=Umax*normrnd(0,1,[1 N]);
v2=Umax*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Umax/st);
mn=mean(v2);st=std(v2);
v2=(v2-mn)*(2-Umax/st);
v=[v1; v2];
clear v1 v2;

v1=Ma*normrnd(0,1,[1 N]);
v2=Ma*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Ma/st);
mn=mean(v2);st=std(v2);
v2=(v2-mn)*(2-Ma/st);
w=[v1 ;v2];
clear v1 v2;

Q=cov(v');
W=cov(w');
%B8=[zeros(6,2);eye(2)];
%sim('Rika',2);
[kalmf,G,]=kalman(Gs,W,Q);

dt=1e-4;
T=0:dt:dt*(N-1);

Dzer=zeros(2,2);
Deye=eye(2);
D=[Dzer Deye;Dzer Dzer];
Gs8=ss(A,[B 0*B],[C;C],D,'inputname',{'Mom1','Mom2','Noise1','Noise2'},'outputname',{'Alpha_Noisy','Beta_Noisy','tAlpha','tBeta'});%������� ��� Simulink
%������ �������

Aest=[A-G*C-B*P];
Best=G;
Cest=[zeros(2,6); eye(6)];
Dest=[Deye; zeros(6,2)];
Kalman=ss(Aest,Best,Cest,Dest,'inputname',{'A1','B2'},'outputname',{'mesAlpha','mesBeta','Alpha','Beta','pAlpha','pBeta','plAlpha','plBeta'});
%�����������
sys=series(Gs8,Kalman,[1 2],[1 2]);
SimModel=feedback(sys,P,[1 2],[3 4 5 6 7 8]);
%SimModel=SimModel([1 2 3 4],[1 2 3 4]);
v=v.*0;
[out,x]=lsim(SimModel,[w' v'],T);
Out=out;
end





