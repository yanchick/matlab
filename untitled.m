% A=[];B=[];C=[];
% for i=1:48
%     if (RR{i}.pow==3)
%         A(RR{i}.k,RR{i}.m)= RR{i}.a;
%     elseif (RR{i}.pow==5)
%         C(RR{i}.k,RR{i}.m)= RR{i}.a;
%     else (RR{i}.pow==7)
%         B(RR{i}.k,RR{i}.m)= RR{i}.a;
%     end
%     
% end
% 
% A(1:2:8,:)=[];
% A(:,1:2:8)=[];
% B(1:2:8,:)=[];
% B(:,1:2:8)=[];
% C(1:2:8,:)=[];
% C(:,1:2:8)=[];
W1=[];
for i=1:5
    tff{i}=tf([1],b.IW{1}(i,:))+b.b{1}(i);   
    if (i<2)
        W1=tff{1}
    else
    W1=parallel(W1,tff{i},[1],[1],[],[]);
    end
end
W1=b.LW{3,2}*(b.LW{2,1}*W1+b.b{2})+b.b{3};