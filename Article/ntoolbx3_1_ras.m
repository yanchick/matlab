function ntoolbx3dem
%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Расчет, одна сеть
%======================================================================
clear;
t=0:0.01:20*pi;
%w=0.1*randn(length(t),1)';
w=sin(2*pi*0.5*t);
ksi=0.7;
w0=1*2*pi;
H=0.12;
A=0.016;
B=0.0005;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];
G=[1; 0 ;0];
C=eye(3);  
D=zeros(3,1);
w0=1;w1=3;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
Mysys=ss((F-G*P),G,C,D);
xf=lsim(Mysys,w,t);
k1_1=6;%Количество слоев задержки по состоянию
k2_1=0;%Количество слоев задержки по упраавлению
k3=3;%Порядок системы
x=[];
Q=[];

%=============================
%Первая подсистема - гироблок
%=============================
y_target=xf;    
y=xf(:,3)'+0.00*max(xf(:,3)')*rand(1,length(xf(:,3)'));

for j=1:k1_1
n=length(y);
z=[zeros(1,j) y(1:n-j)];
x=[x; z];
Q=[Q ;min(y) max(y)];
end

%plot(t,sin(t),t,x);

net3_1=newff(Q,[5 3],{'purelin' 'purelin'},'trainlm');
net3_1.trainParam.show = 2;
net3_1.trainParam.epochs = 5000;
net3_1.trainParam.goal = 1e-7;
net3_1.trainParam.min_grad=1e-20;
net3_1=train(net3_1,x,y_target');
end

