t=0:0.01:60; 
jj=1;

for pow=3:2:7
p= (t.^pow*90)/(60^pow);

for k=2:2:8
    for m=2:2:8
k1=k;%Количество слоев задержки по состоянию
k2=0;%Количество слоев задержки по состоянию
x=t+0.5*randn(1,length(t));
y=p;
in=[];
out=[];
Q=[];

%%%%%%%%%
%Вход
%%%%%%%%%
for j=0:k1
n=length(x);
z=[zeros(1,j) x(1:n-j)];
in=[in; z];
Q=[Q ;min(x) max(x)];
end


%%%%%%%%%
%Выход
%%%%%%%%%
for j=1:k2
n=length(y);
zl=[zeros(1,j) y(1:n-j)];
out=[out; y];
Q=[Q ;min(y) max(y)];
end


%%%%%%%%
%Обучение
%%%%%%%%

net3=newff(Q,[m 1],{'tansig' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs = 1000;
net3.trainParam.goal = 1e-5;
net3.trainParam.min_grad=1e-100;
net3.trainParam.mu_max=1e100;
[net3]=train(net3,[in],y);k1=k1+1;

P = 1.0;Q=1e-5; R=0.5;
qw=[]; qqw=[];qqqw=[];qqqqw=[];InNet1=zeros(k1,1);tin1=InNet1; xh=[];
xhat = 0.0;
for i=60:-0.01:0;
    z=i+4*rand(1);
    tin1=[z; tin1];    
    InNet1=tin1(1:k1);
    qw=[qw sim(net3,InNet1)]; %NNET
    qqw=[qqw (z^pow*90)/(60^pow)];%Z
    qqqw=[qqqw  (i^pow*90)/(60^pow)];%x
    xhatminus = xhat;
    Pminus = P+Q;
    K= Pminus/( Pminus+R ); 
    xhat = xhatminus+K*(z-xhatminus);
    P= (1-K)*Pminus;
    qqqqw=[qqqqw (xhat^pow*90/(60^pow))];%Kalman
end
RR{jj}.qw=qw;RR{jj}.qqw=qqw;RR{jj}.qqqw=qqqw;RR{jj}.qqqqw=qqqqw;
RR{jj}.a=std(qqqw-qw);RR{jj}.b=std(qqqw-qqw);RR{jj}.c=std(qqqw-qqqqw);
RR{jj}.k=k;RR{jj}.pow=pow;RR{jj}.m=m;
%file=strcat('neural_',num2str(pow),'_',num2str(k),'.cpp');
%GenArduinoNN(net3,file)
jj=jj+1;
    end
end
end

A=[];B=[];C=[];
for i=1:48
    if (RR{i}.pow==0.1)
        A(RR{i}.k,RR{i}.m)= RR{i}.a;
    elseif (RR{i}.pow==0.5)
        C(RR{i}.k,RR{i}.m)= RR{i}.a;
    else (RR{i}.pow==0.3)
        B(RR{i}.k,RR{i}.m)= RR{i}.a;
    end
    
end

A(1:2:8,:)=[];
A(:,1:2:8)=[];
B(1:2:8,:)=[];
B(:,1:2:8)=[];
C(1:2:8,:)=[];
C(:,1:2:8)=[];