function [OutXfk,OutXns, OutNbfk, 
%=======================================================================
%Основная исследовательска прога
%Двуосник с 6 сетками
%======================================================================
tt=load('rd.mat');%��������� ����� �� �����
load('nets.mat')
tt.w0=tt.w0/7.5;
A = [              0                       0                     0                   0                         1                            0          -1                          0;  
                    0                       0                     0                   0                         0                            1           0                         -1;                 
                    1                       0                     0                   0                         0                            0           0                          0;  
                    0                       1                     0                   0                         0                            0           0                          0;                 
                    0                   tt.kd*tt.w0                 0                   0                    -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)                   0   tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
              tt.kd*tt.w0                     0                     0                   0       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b)     -tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b)     0;      
                    0                       0                     0                   0                        0                             0     -tt.h0/tt.A0                        0; 
                    0                       0                     0                   0                        0                             0          0                       -tt.h0/tt.B0  ];
% A(:,3:4)=[];
% A(3:4,:)=[];
niz=2;
np=8;
B=[zeros(np-niz,2);eye(2)];
C=[eye(2) zeros(2,np-niz)] ;
w0=20;w2=30;w3=24;w1=27;ksi=0.9;
goal1=sort( [desir(w0,ksi) desir(w1,ksi) desir(w2,ksi) desir(w3,ksi)]);
P=place(A,B,goal1);


Ma=0.4;%��� �������
Umax=0.001*pi/(180*60);%��� ����
Q=[Umax 0;0 Umax];
W=[Ma 0;0 Ma];

Sig=zeros(np);

dt0=1e-5;
Sigp=[];
for t=0:dt0:0.5
   dSig=A*Sig+Sig*transpose(A)-Sig*transpose(C)*inv(Q)*C*Sig+B*W*transpose(B);  
   Sig=Sig+dSig*dt0;
   Sigp=[Sigp Sig(1,1)];
end
K=Sig*transpose(C)*inv(Q);

k1_1=21;%Количество слоев задержки по состоянию
k1_2=2;%Количество слоев задержки по упраавлению

% % x0=zeros(12,1);x=x0;xe=x;xpr=x;xpre=xe;
% % alpha=[0];alphapr=0;u0=0;xest=zeros(6,1);xwcap=[0;0;0];
% % Sqerr=[0;0;0];ue=[];ux=[];uex=[0;0];;sqerr=[];sqerr2=[];

x_ns=zeros(np,1);ex_ns=zeros(np,1);xest_ns=zeros(np,1);i=1;
OutX_ns=[];u1_ns=[];Out11_ns=[];xestv_ns=[];u_ns=zeros(2,1);

x_fk=zeros(np,1);ex_fk=zeros(np,1);xest_fk=zeros(np,1);i=1;
OutX_fk=[];u1_fk=[];Out11_fk=[];xestv_fk=[];u_fk=zeros(2,1);

InputY1=zeros(1,k1_1);
InputY2=zeros(1,k1_1);

dt=5e-5; Tend=1.0
w=[0;0];
kv=14;
net10=nnsetka(net1,kv);
net20=nnsetka(net2,kv);
net30=nnsetka(net3,kv);
net40=nnsetka(net4,kv);
net50=nnsetka(net5,kv);
net60=nnsetka(net6,kv);
net70=nnsetka(net7,kv);
net80=nnsetka(net8,kv);

for t=0:dt:Tend 
  w=[rand(1); rand(1)];
% tt.w0=tt.w0/7.5+4.5*rand(1);
% A = [              0                       0                     0                   0                         1                            0          -1                          0;  
%                     0                       0                     0                   0                         0                            1           0                         -1;                 
%                     1                       0                     0                   0                         0                            0           0                          0;  
%                     0                       1                     0                   0                         0                            0           0                          0;                 
%                     0                   tt.kd*tt.w0                 0                   0                    -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)                   0   tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
%               tt.kd*tt.w0                     0                     0                   0       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b)     -tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b)     0;      
%                     0                       0                     0                   0                        0                             0     -tt.h0/tt.A0                        0; 
%                     0                       0                     0                   0                        0                             0          0                       -tt.h0/tt.B0  ];
% 
%ГС на базе НС    
% nmmodel('schem1.mat',nets20,20,w)
% nmmodel('schem2.mat',nets18,18,w)
% nmmodel('schem3.mat',nets16,16,w)

% i=i+1;
%Cетка на базе НС    
  u_ns=-P*xest_ns;%Управление
  u1_ns=[u1_ns u_ns];
  dx_ns=A*x_ns+B*(u_ns+w);%Основная модель 
  x_ns=x_ns+dx_ns*dt;%Перменная состояния объекта
  y_ns=C*x_ns;%Наблюдение  
   
  InputY1=[y_ns(1) InputY1];
  InputY1=InputY1(1:(k1_1));
  InputY2=[y_ns(2) InputY2];
  InputY2=InputY2(1:(k1_1));
  Input=[InputY1' ; InputY2'];  
  y1=sim(net10,Input);
  y2=sim(net20,Input);
  y3=sim(net30,Input);
  y4=sim(net40,Input);
  y5=sim(net50,Input);
  y6=sim(net60,Input);
  y7=sim(net70,Input);
  y8=sim(net80,Input); 
  xest_ns= [y1 y2 x_ns(3:4)' y5 y6 y7 x_ns(8)' ]';
%  xest_ns= [y1 y2 y3 y4 y5 y6 y7 x_ns(8)' ]';  
  Out11_ns=[Out11_ns xest_ns];
  %Определение праметров наблюдения
  OutX_ns=[OutX_ns x_ns];
%==============
% ГС на базе ФКc
%==============
  u_fk=-P*ex_fk;%Управление
  u1_fk=[u1_fk u_fk];
  y_fk=C*x_fk;%Наблюдение  
  Delt_x_fk=y_fk-C*ex_fk;%Разность между выходом фильтра и измерен
  dx_fk=A*x_fk+B*(u_fk+w);%Основная модель 
  dex_fk=kvant(A,kv)*ex_fk+kvant(B,kv)*u_fk+kvant(K,kv)*Delt_x_fk;%Фильтр
  x_fk=x_fk+dx_fk*dt;%Перменная состояния объекта
  exlast_fk=ex_fk;
  ex_fk=ex_fk+dex_fk*dt;%Переменная состояния фильтра  
  Out11_fk=[Out11_fk ex_fk]; 
  %Определение праметров наблюдения
  OutX_fk=[OutX_fk x_fk];
t
end