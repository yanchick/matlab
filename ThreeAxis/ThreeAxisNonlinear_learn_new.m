function [net1,net2,net3]=ThreeAxisNonlinear(k1,n1,n2)
%%%%%%%%%%%%%%%%%%%%%%%
%%Мегамодель с нелинейностями
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x=zeros(6,1);dx=zeros(6,1);
ksi=0.7;
w0=1*2*pi;
h=0.05;

Jxp = 0.015;
Jyp = 0.014;
Jzp = 0.016;


Jzvr = 0.003;
Jyvr = 0.006;
Jxvr = 0.003;

Jxnr = 0.0055;
Jynr = 0.0025;
Jznr = 0.0025;
m1=0.1;m2=0.1;m3=0.1;


A1=Jynr+Jyvr*cos(x(2))^2+Jzvr*sin(x(2))^2+Jyp*cos(x(2))^2*cos(x(3))^2+Jxp*cos(x(3))^2*sin(x(3))^2
A2=Jxvr+Jxp*cos(x(3))^2+Jyp*sin(x(3))^2
A3=Jzp



% dxz(1) = -1*(h*x(1)-0.5*(Jyp-Jxp)*dx(2)*cos(x(5))*sin(2*x(5)))/A1 + m1
% dxz(2) = -1*(h*x(2)+0.5*(Jyp-Jxp)*dx(1)*cos(x(5))*sin(2*x(5)))/A2 + m2
% dxz(3) = -1*(h*x(3))/A3 + m3
%
%
% %Итерация 1
% dx(1) = -1*(h*x(1)-0.5*(Jyp-Jxp)*dxz(2)*cos(x(5))*sin(2*x(5)))/A1 + m1
% dx(2) = -1*(h*x(2)+0.5*(Jyp-Jxp)*dxz(1)*cos(x(5))*sin(2*x(5)))/A2 + m2
% dx(3) = -1*(h*x(3))/A3 + m3
% dx(4) = x(1)
% dx(5) = x(2)
% dx(6) = x(3)


tt=load('rd.mat');%
eps=0;
A0 = [              0                       0                        1                            0   ;
    0                       0                        0                            1   ;
    eps                   tt.kd*tt.w0          -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b);
    tt.kd*tt.w0                   eps       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b); ];


Pp1=tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
% Pp2=
G=[1 0 0;
    0 1 0;
    0 0 1;
    0 0 0;
    0 0 0;
    0 0 0;];
% C=[0 0 0 0 0 0];
% D=zeros(6,1);
w0=1;w1=3; ksi=0.8;
goal=sort([desir(w0,ksi) -1/w1 desir(w0,ksi) -1/(w1)]);
F= [ -1*(h)/A1 0 0  0 0 0;
    0 -1*(h)/A2 0  0 0 0;
    0  0 -1*(h)/A3 0 0 0;
    1 0 0          0 0 0;
    0 1 0          0 0 0;
    0 0 1          0 0 0 ] ;
P=place(F,G,goal);


dt=5e-3;Tend=24;
x=zeros(6,1);dx=zeros(6,1);u=zeros(3,1)
xt=zeros(6,1);
xtz=[];xttz=[];xt(5)=0*pi/180;xt(6)=10*pi/180;ut=[];
f=5;
for t=0:dt:Tend;
    u=-P*x;
    
    A1=Jynr+Jyvr*cos(xt(5))^2+Jzvr*sin(xt(5))^2+Jyp*cos(xt(5))^2*cos(xt(6))^2+Jxp*cos(xt(5))^2*sin(xt(6))^2;
    A2=Jxvr+Jxp*cos(xt(6))^2+Jyp*sin(xt(6))^2;
    A3=Jzp;
    dx(1) = -1*(h*x(1)-0.5*(Jyp-Jxp)*dx(2)*cos(xt(5))*sin(2*xt(5)))/A1 + 0.2*(sin(2*pi*f*t)+ 0.0*randn(1)) +u(1);
    dx(2) = -1*(h*x(2)+0.5*(Jyp-Jxp)*dx(1)*cos(xt(5))*sin(2*xt(5)))/A2 +0.2*(sin(2*pi*f*t)+ 0.0*randn(1))+u(2);
    dx(3) = -1*(h*x(3)-Jznr*dx(1)*sin(xt(6)))/A3 +  0.2*sin(2*pi*f*t) +u(3);
    dx(4) = x(1)*cos(xt(5))*cos(xt(6))-x(2)*sin(xt(6));
    dx(5) = x(2)*cos(xt(6))+dx(1)*cos(xt(5))*sin(xt(6));
    dx(6) = x(3)-x(1)*sin(xt(6));
    x=x+dx*dt;
    xt=xt+dx*dt;
    xtz=[xtz x];
    xttz=[xttz xt];
    ut=[ut u];
end
In0=[sin(xttz(6,:));cos(xttz(6,:))];
Q0=[-1 1;-1 1];

[In1,Q1]=TDLDataGen(xtz(4,:),k1);
In1=[In0;In1];
Q1=[Q0;Q1];
[In2,Q2]=TDLDataGen(xtz(5,:),k1);
In2=[In0;In2];
Q2=[Q0;Q2];
[In3,Q3]=TDLDataGen(xtz(6,:),k1);
In3=[In0;In3];
Q3=[Q0;Q3];

f=[n1 n2 1];
[net1]=newff(Q1,f,{'tansig' 'purelin' 'purelin'},'trainlm');
net1.trainParam.show = 2;
net1.trainParam.epochs = 7500;
net1.trainParam.goal = 5e-9;
net1.trainParam.min_grad=1e-20;
net1=train(net1,In1,ut(1,:));

[net2]=newff(Q2,f,{'tansig' 'purelin' 'purelin'},'trainlm');
net2.trainParam.show = 2;
net2.trainParam.epochs = 7500;
net2.trainParam.goal = 5e-9;
net2.trainParam.min_grad=1e-20;
net2=train(net2,In2,ut(2,:));

[net3]=newff(Q3,f,{'tansig' 'purelin' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs = 7500;
net3.trainParam.goal = 5e-9;
net3.trainParam.min_grad=1e-20;
net3=train(net3,In3,ut(3,:));

% 
% dt=5e-3;Tend=20;
% x=zeros(6,1);dx=zeros(6,1);u=zeros(3,1)
% xt=zeros(6,1);
% xtz=[];xttz=[];xt(4)=al1*pi/180;xt(5)=al2*pi/180;xt(6)=al3*pi/180;ut=[];
% InNet1=zeros(1,k1);InNet2=zeros(1,k1);InNet3=zeros(1,k1);
% u=[0;0;0];
% for t=0:dt:Tend;
%     A1=Jynr+Jyvr*cos(xt(5))^2+Jzvr*sin(xt(5))^2+Jyp*cos(xt(5))^2*cos(xt(6))^2+Jxp*cos(xt(5))^2*sin(xt(6))^2;
%     A2=Jxvr+Jxp*cos(xt(6))^2+Jyp*sin(xt(6))^2;
%     A3=Jzp;
%     
%     dx(1) = -1*(h*x(1)-0.5*(Jyp-Jxp)*dx(2)*cos(xt(5))*sin(2*xt(5)))/A1 + m1 +u(1)*cos(xt(6))+u(2)*sin(xt(6));
%     dx(2) = -1*(h*x(2)+0.5*(Jyp-Jxp)*dx(1)*cos(xt(5))*sin(2*xt(5)))/A2 + m2 +u(2)*cos(xt(6))-u(1)*sin(xt(6));
%     dx(3) = -1*(h*x(3)-Jznr*dx(1)*sin(xt(6)))/A3+ m3 +u(3);
%     w1=x(1)*cos(xt(5))*cos(xt(6))-x(2)*sin(xt(6));
%     w2=x(2)*cos(xt(6))+dx(1)*cos(xt(5))*sin(xt(6));
%     w3= x(3)-x(1)*sin(xt(6));
%     dx(4) = w1;
%     dx(5) = w2;
%     dx(6) = w3;
%     x=x+dx*dt;
%     InNet1=[x(4) InNet1];
%     InNet1=InNet1(1:k1);
%     InNet2=[x(5) InNet2];
%     InNet2=InNet2(1:k1);
%     InNet3=[x(6) InNet3];
%     InNet3=InNet3(1:k1);
%     xt=xt+dx*dt;
%     xtz=[xtz rad2deg(x(4:6))*60];
%     xttz=[xttz rad2deg(xt(4:6))*60];
%     
%     u=[sim(net1,[InNet1']);
%         sim(net2,[InNet2']);
%         sim(net3,[InNet3']);];
%     
%     ut=[ut u];
% end
% al0=xttz;
% dal0=xtz;

end




