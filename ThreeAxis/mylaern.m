%%%%%%%%%%%%%%%%%%%%%%%
%%Мегамодель с нелинейностями
%%Моделирование
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x=zeros(6,1);dx=zeros(6,1);
ksi=0.7;
w0=1*2*pi;
h=0.05;

Jxp = 0.015;
Jyp = 0.014;
Jzp = 0.016;


Jzvr = 0.003;
Jyvr = 0.006;
Jxvr = 0.003;

Jxnr = 0.0055;
Jynr = 0.0025;
Jznr = 0.0025;
m1=0.1;m2=0.1;m3=0.1;


A1=Jynr+Jyvr*cos(x(2))^2+Jzvr*sin(x(2))^2+Jyp*cos(x(2))^2*cos(x(3))^2+Jxp*cos(x(3))^2*sin(x(3))^2
A2=Jxvr+Jxp*cos(x(3))^2+Jyp*sin(x(3))^2
A3=Jzp



% dxz(1) = -1*(h*x(1)-0.5*(Jyp-Jxp)*dx(2)*cos(x(5))*sin(2*x(5)))/A1 + m1
% dxz(2) = -1*(h*x(2)+0.5*(Jyp-Jxp)*dx(1)*cos(x(5))*sin(2*x(5)))/A2 + m2
% dxz(3) = -1*(h*x(3))/A3 + m3
%
%
% %Итерация 1
% dx(1) = -1*(h*x(1)-0.5*(Jyp-Jxp)*dxz(2)*cos(x(5))*sin(2*x(5)))/A1 + m1
% dx(2) = -1*(h*x(2)+0.5*(Jyp-Jxp)*dxz(1)*cos(x(5))*sin(2*x(5)))/A2 + m2
% dx(3) = -1*(h*x(3))/A3 + m3
% dx(4) = x(1)
% dx(5) = x(2)
% dx(6) = x(3)


tt=load('rd.mat');%
eps=0;
A0 = [              0                       0                        1                            0   ;
    0                       0                        0                            1   ;
    eps                   tt.kd*tt.w0          -tt.kd/(tt.B_+tt.b)  (tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b);
    tt.kd*tt.w0                   eps       -(tt.A_+2*tt.b)*tt.w0/(tt.B_+tt.b)             -tt.kd/(tt.B_+tt.b); ];


Pp1=tt.w0*(tt.A_+tt.a+tt.b-tt.c)/(tt.B_+tt.b);
% Pp2=
G=[1 0 0;
    0 1 0;
    0 0 1;
    0 0 0;
    0 0 0;
    0 0 0;];
C=[0 0 0 0 0 0];
D=zeros(6,1);
w0=1;w1=3; ksi=0.8;
goal=sort([desir(w0,ksi) -1/w1 desir(w0,ksi) -1/(w1)]);
F= [ -1*(h)/A1 0 0  0 0 0;
    0 -1*(h)/A2 0  0 0 0;
    0  0 -1*(h)/A3 0 0 0;
    1 0 0          0 0 0;
    0 1 0          0 0 0;
    0 0 1          0 0 0 ] ;
P=place(F,G,goal);

dt=5e-3;Tend=20;
x=zeros(6,1);dx=zeros(6,1);u=zeros(3,1)
xt0=zeros(6,1);
xtz=[];xttz=[];xt(4)=al1*pi/180;xt0(5)=al2*pi/180;xt0(6)=al3*pi/180;ut=[];
InNet01=zeros(1,k1);InNet02=zeros(1,k1);InNet03=zeros(1,k1);xt=xt0;
u=[0;0;0];
for t=0:dt:Tend;
    A1=Jynr+Jyvr*cos(xt0(5))^2+Jzvr*sin(xt0(5))^2+Jyp*cos(xt0(5))^2*cos(xt0(6))^2+Jxp*cos(xt0(5))^2*sin(xt0(6))^2;
    A2=Jxvr+Jxp*cos(xt0(6))^2+Jyp*sin(xt0(6))^2;
    A3=Jzp;
    
    dx(1) = -1*(h*x(1)-1*0.5*(Jyp-Jxp)*dx(2)*cos(xt0(5))*sin(2*xt0(5)))/A1 + 0.1 +u(1)*cos(xt0(6))-u(2)*sin(xt0(6));
    dx(2) = -1*(h*x(2)+1*0.5*(Jyp-Jxp)*dx(1)*cos(xt0(5))*sin(2*xt0(5)))/A2 + 0.1 +u(2)*cos(xt0(6))+u(1)*sin(xt0(6));
    dx(3) = -1*(h*x(3)-1*Jznr*dx(1)*sin(xt0(6)))/A3+ 0.1 +u(3);
    w1=x(1)*cos(xt0(5))*cos(xt(6))-x(2)*sin(xt0(6));
    w2=x(2)*cos(xt0(6))+x(1)*cos(xt0(5))*sin(xt0(6));
    w3= x(3)-1*x(1)*sin(xt0(6));
    dx(4) = w1;
    dx(5) = w2;
    dx(6) = w3;
    x=x+dx*dt;
    xt=xt+dx*dt;
    
    InNet01=[x(4) InNet01];
    InNet01=InNet01(1:k1);
    InNet02=[x(5) InNet02];
    InNet02=InNet02(1:k1);
    InNet03=[x(6) InNet03];
    InNet03=InNet03(1:k1);
    
%     InNet1=[[sin(xt(6)) cos(xt(6))] InNet01]; 
%     InNet2=[[sin(xt(6)) cos(xt(6))] InNet02]; 
%     InNet3=[[sin(xt(6)) cos(xt(6))] InNet03]; 
    
    InNet1=[InNet01]; 
    InNet2=[InNet02]; 
    InNet3=[InNet03];        
    xtz=[xtz rad2deg(x(4:6))*60];
    xttz=[xttz rad2deg(xt(4:6))*60];
    
    u1=[sim(n1,[InNet1']);
        sim(n2,[InNet2']);
        sim(n3,[InNet3']);];
    
    u2=-1*P*x;
  % u=[u1(1:2);u2(3)];
    u=u1;
    ut=[ut u];
end
al0=xttz;
dal0=xtz;
