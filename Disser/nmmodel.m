function nmmodel(filename,nets,k1_1_1,w)
i=1;
load(filename);
net1=nets(1);
net2=nets(2);
net3=nets(3);
net4=nets(4);
net5=nets(5);
net6=nets(6);

if (i==1)
   InputY1=zeros(1,(k1_1_1+1));
   InputY2=zeros(1,(k1_1_1+1));
   x_ns=zeros(6,1);ex_ns=zeros(6,1);xest_ns=zeros(6,1);i=1;
   OutX_ns=[];OutEx_ns=[];q_ns=[];West_ns=[];
   u1_ns=[];Out11_ns=[];als_ns=[];xestv_ns=[];Outx_ns=[];u_ns=zeros(2,1);
end
i=i+1;
%Cетка на базе НС    
 u_ns=-P*xest_ns;%Управление
 u1_ns=[u1_ns u_ns];
  dx_ns=A*x_ns+B*(u_ns+w);%Основная модель 
  x_ns=x_ns+dx_ns*dt;%Перменная состояния объекта
  y_ns=C*x_ns;%Наблюдение  
   
  InputY1=[y_ns(1) InputY1];
  InputY1=InputY1(1:(k1_1_1+1));
  InputY2=[y_ns(2) InputY2];
  InputY2=InputY2(1:(k1_1_1+1));
  Input=[InputY1' ; InputY2'];
  
  y1=sim(net1,Input);
  y2=sim(net2,Input);
  y3=sim(net3,Input);
  y4=sim(net4,Input);
  y5=sim(net5,Input);
  y6=sim(net6,Input);
  xest_ns= [y1 y2 y3 y4 y5 y6]';
  Out11_ns=[Out11_ns xest_ns];
  %Определение праметров наблюдения
OutX_ns=[OutX_ns x_ns];
save(filename);
end
