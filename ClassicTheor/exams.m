clear all;
dt=5e-4;%��� ��������������
T0=5;%����� ��������������
Nstep=1000;%����� ������
Jk=3000;%����� ������������
T=0:dt:T0;
N=length(T);
H=1;
A=0.1;
B=0.05;

H=0.12;
A=0.016;
B=8e-5;

F=[0  -H/A 0;
   H/B   0 0;
   0     1 0;];%������� ���������
G=[1; 0 ;0]; %������� ����������
C=[0 0 1];  %������� ����������
w0=5;w1=10;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
%������������ ��������� �������
Ma=0.4;%��� �������
Umax=1*0.1*pi/(180*60);%��� ����
%������������� ���
v1=Umax*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Umax/st);
v=v1;
clear v1;
%������������� ���������� ����������� �� ��� ��
v1=Ma*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Ma/st);
w=v1;
clear v1 v2;
%������������ ������������� ������
Q=Umax^2; 
W=Ma^2;
Sig=zeros(3);
%������� ��������� �������
dt0=0.0001;
Sigp=[];
for t=0:dt0:0.3
   dSig=F*Sig+Sig*transpose(F)-Sig*transpose(C)*inv(Q)*C*Sig+G*W*transpose(G);  
   Sig=Sig+dSig*dt0;
   Sigp=[Sigp Sig(1,1)];
end
K=Sig*transpose(C)*inv(Q);
%������������� �������
x=zeros(3,1);ex=zeros(3,1);
i=1;
OutX=[];OutEx=[];q=[];West=[];Sig_est=Sig;Wvar=W;
Jmp=5;xlast=0;Sr=[];alx=0;Shum=[];Wful=[];F1=[];F2=[];
als=[];
for t=0:dt:T0
 u=-P*ex;%����������
  y=C*x+v(i);%����������  
  Delt_x=y-C*ex;%�������� ����� ������� ������� � �������
  dx=F*x+G*(u+Jmp*w(i));%�������� ������ 
  dex=F*ex+G*u+K*Delt_x;%������
  x=x+dx*dt;%��������� ��������� �������
  exlast=ex;
  ex=ex+dex*dt;%���������� ��������� �������
  %��������� ������
  detx=F*ex+G*u;%�������� ������  
  %����������� ��������� ����������
  F1=[F1 F*x+G*u];
  F2=[F2 F*ex+G*u];
  q=[1 0 0]*(y); %������� �������� ����
  q1=[1 0 0]*(dx-F*x-G*u);
  West=[West q]; %���������� ��� � ������
  Wful=[Wful q1];
OutX=[OutX x];
OutEx=[OutEx ex];
Shum=[Shum Jmp*w(i)];
i=i+1;
end
%plot (T,Shum(1,:),T,rad2deg(OutX(3,:))*60); figure(gcf)
