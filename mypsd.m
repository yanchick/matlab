function Sxx = mypsd(x,w0,dw,N,dtau)
Sxxtemp=zeros(w0/dw+1,1);
K_temp=myautocorr(x',N);
%dtau=0.1;%2*pi/dw;
for w=0:w0/dw    
   for tau=0:N-1
    Sxxtemp(w+1) = Sxxtemp(w+1) + K_temp(tau+1)*cos(w*dw*tau*dtau)*dtau;
    end
end;
%R=xcorr(x,'biased');
Sxx=2*Sxxtemp;
end