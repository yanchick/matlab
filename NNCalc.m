function [net, time,tr]=NNcalc(k1,n1,x_noised,x)
%=======================================================================
%Нейро фильтр расчет
%======================================================================
x_noised_bank=[];
Q = [];
for j=0:k1
    n=length(x_noised);
    x_noised_temp=[zeros(1,j) x_noised(1:n-j)];
    x_noised_bank=[x_noised_temp; x_noised_bank];
    Q=[Q ;min(x_noised) max(x_noised)];
end
%[a b] = size(x_noised_bank)
t=cputime;
fu={'tansig' 'purelin'};
net=newff(Q,[n1 1],fu,'trainlm');
net.trainParam.show = 2;
net.trainParam.epochs = 2500;
net.trainParam.goal = 1e-5;
net.performFcn = 'mse'
%net3.trainParam.min_grad=1e-20;
[net tr]=train(net,x_noised_bank,x);
time=cputime-t;
end