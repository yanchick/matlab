global x
clear all;
dt=1e-4;%Шаг интегрирования
T0=7.5;%Время интегрирования
T=0:dt:T0;
N=length(T);
H=0.12;
A=0.016;
B=8e-5;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];%Матрица состояния
G=[1; 0 ;0]; %Матрица Управления
C=[0 0 1];  %Матрица наблюдения
w0=2;w1=1;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
%Формирования случайных величин
Ma=0.4;%СКО момента
Umax=1*0.1*pi/(180*60);%СКО шума
%Измерительный Шум
v1=Umax*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Umax/st);
v=v1;
clear v1;
%Форимирование случайного воздействия по оси ГС
v1=Ma*normrnd(0,1,[1 N]);
mn=mean(v1);st=std(v1);
v1=(v1-mn)*(2-Ma/st);
w=v1;
clear v1 v2;
%Формирования корреляционых матриц
Q=Umax^2; 
W=Ma^2;
Pxx=zeros(3);
Pxa=zeros(3,1);
Pax=Pxa';       
Paa=zeros(1);
Pxxt=[];Pxat=[];Paat=[];
%Решение уравнения Риккати


fx=[0   -H/B 0 ;
  H/A   0  0 ;
    0    1  0];
fh=[ -1/B; 1/A;  0 ];
[nt mt]=size(C);
dt=1e-4;Sigp=[];Sig=zeros(3);
for t=0:dt:0.5
   dPxx=F*Pxx+Pxx*transpose(F)-Pxx*transpose(C)*inv(Q)*C*Pxx+G*W*transpose(G);  
   Pxx=Pxx+dPxx*dt;
   Pxxt=[Pxxt Pxx(1,1)];
end
K=Sig*transpose(C)*inv(Q);
% Моделирование Системы
x=zeros(3,1);ex=zeros(3,1);
i=1;h=0;OutH=[];
OutX=[];OutEx=[];q=[];West=[];Wvar=W;
Jmp=5;xlast=0;Sr=[];alx=0;Shum=[];Wful=[];F1=[];F2=[];
u1=[];
als=[];
C=[0 0 1];

dt=1e-4;
for t=0:dt:T0
    H=0.12*(1+0.5*randn(1));
    F=[0  -H/B 0;
      H/A   0 0;
       0    1 0;];%Матрица состояния
    u=-P*ex;%Управление
    u1=[u1 u];
    y=C*x;%Наблюдение  
    Delt_x=y-C*ex;%Разность между выходом фильтра и измерен
    dx=F*x+G*(u+1);%Основная модель
    dex=F*ex+G*u+Pxx*C'*inv(Q)*(y-C*ex);%Фильтр
    x=x+dx*dt;%Перменная состояния объекта
    ex=ex+dex*dt;%Переменная состояния фильтра
    %Определение праметров наблюдения
    OutX=[OutX x];
    OutH=[OutH h];
    i=i+1;
end
