function [mx,mdn,tpp]=GetpEstPP(x)
mx=max(abs(x'))';
mdn=mean(x(0.85*length(x):length(x)-1)')';
mxn=max(abs(x(0.85*length(x):length(x)-1)'))';
tpp=-1;
%for j=1:a
if (abs(mdn)<1e-6)
for i=length(x)-1:-1:2
    if abs(x(i))>1e-2
        tpp = i;
        break
    end
end   
else
for i=length(x)-1:-1:1
    if abs(x(i)-mdn)>mxn
        tpp = i;
        break
    end
end
end
%end
tpp=tpp';
end
