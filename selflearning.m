clear;
x0=0:0.01:2*pi;
y=cos(x0);
Plo=[]; 
eta=0.25;
mu=0.9;

m=1;%размерность входа
n=[m 5 1];%количество нейроннов
func={@tansig, @tansig};%Функции активации

for i=1:length(n)-1
W{i}=20.*randn(n(i+1),n(i)+1);%> %Веса
end

wp=[];
wp=[wp W{1}(1,1)];
    for i=1:50    
    z=randperm(length(y));
    Errs=0;
    if i>2 
        eta=Plo(i-1)/Plo(i-2);
    end
    for i=1:length(y)
        l=z(i);
        Z{1}=[1;x0(l)];
        %Прямой проход
        for j=1:length(n)-1
            i=j+1;
            Z{i}=[1;func{j}((W{j}*Z{j}))];
        end
        Z{length(n)}=Z{length(n)}(2:n(length(n))+1);
        err=Z{length(n)}-y(l);
        Errs=Errs+err^2;
        d=0; 
        for is=1:length(n)-1
              delt{is}=[];
              dW{is}=W{is}*0;
             pdW{is}=W{is}*0;

        end

     %Обратный проход
     for j=length(n):-1:2
      i=j-1;%номер слоя

      if j==length(n)%Выходной слой
           delt{i} = func{i}('dn',W{i}*Z{i})*err;
           for p=1:length(delt{i})
               for q=1:length(Z{i})
                   dW{i}(p,q) = -eta*delt{i}(p)*Z{i}(q);
                  % dW{i}(p,q) = -eta*(mu*pdW{i}(p,q)+(1-mu)*delt{i}(p)*Z{i}(q));

               end 
           end
           pdw{i}=dW{i};
      else             %Скрытый слой
          tmp=[];      
          for s=1:(n(j))
            tmp=func{i}('dn',func{i}(W{i}*Z{i}));
            delt{i}(s)=tmp(s)*sum(W{i+1}(:,s).*delt{i+1}');
          end           
          %tmp=W{i+1}.*delt{i+1};
          %tmp= delt{d-1}(2:length(delt{d-1}))*W{i}(z,:);
          %delt{i} =func{i}('dn',func{i}(W{i}*Z{i}))*tmp'
          for p=1:length(delt{i})
               for q=1:length(Z{i})
                  dW{i}(p,q) = -eta*delt{i}(p)*Z{i}(q);
                 %dW{i}(p,q) = -eta*(mu*pdW{i}(p,q)+(1-mu)*delt{i}(p)*Z{i}(q));

               end               
          end
          pdw{i}=dW{i};

      end
      wp=[wp W{1}(1,1)];
    end
    for q=1:length(n)-1
        W{i}=W{i}+dW{i};
    end
    end
    Plo=[Plo Errs/length(y)];
    end