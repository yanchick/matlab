%Нейронка
%==================================================== 

function neunet=neural11(out,k)
%t=load('matlab.mat');
%[y_target xf]=preparedata(0.00005,1);
y_target=out;
xf=out(1:2,:)';
k1_1=k;%Количество слоев задержки по состоянию
k1_2=2;%Количество слоев задержки по упраавлению
k3=3;%Порядок системы
x=[];
Q=[];
 
%=============================
%Канал №1
%=============================
%y_target=xf;    

y1=xf(:,1)';

for j=0:(k1_1)
n=length(y1);
z=[zeros(1,j) y1(1:n-j)];
x=[x; z];
Q=[Q ;min(y1') max(y1')];
end

y2=xf(:,2)';

for j=0:(k1_1)
n=length(y2);
z=[zeros(1,j) y2(1:n-j)];
x=[x; z];
Q=[Q ;min(y2') max(y2')];
end

% y1=y_target(:)';
% 
% for j=1:k1_2
% n=length(y1);
% z=[zeros(1,j) y1(1:n-j)];
% x=[x; z];
% Q=[Q ;min(y1') max(y1')];
% end
% 
% y1=y_target(:,4)';
% 
% for j=1:k1_2
% n=length(y1);
% z=[zeros(1,j) y1(1:n-j)];
% x=[x; z];
% Q=[Q ;min(y1') max(y1')];
% end
% 
% y1=y_target(:,5)';
% 
% for j=1:k1_2
% n=length(y1);
% z=[zeros(1,j) y1(1:n-j)];
% x=[x; z];
% Q=[Q ;min(y1') max(y1')];
% end
% 
% y1=y_target(:,6)';
% 
% for j=1:k1_2
% n=length(y1);
% z=[zeros(1,j) y1(1:n-j)];
% x=[x; z];
% Q=[Q ;min(y1') max(y1')];
% end
% 

%plot(t,sin(t),t,x);

net3_1=newff(Q,[5 1],{ 'purelin' 'purelin'},'trainlm');
net3_1.trainParam.show = 2;
net3_1.trainParam.epochs = 4000;
net3_1.trainParam.goal = 1e-15;
net3_1.trainParam.min_grad=1e-200;
net3_1.trainParam.mu_max=1e100;
net3_1=train(net3_1,x,y_target);
neunet=net3_1;
end

function [Xout Yin]=preparedata(dt,Tend)
t=load('rd.mat');%��������� ����� �� �����
global A8 C8out B8 Q W G1 P;

Ma=0.4;%��� �������
Umax=1*0.1*pi/(180*60);%��� ����

A = [              0                       0                     0                   0                         1                            0          -1                          0;
                   0                       0                     0                   0                         0                            1           0                         -1;
                   1                       0                     0                   0                         0                            0           0                          0;
    0                       1                     0                   0                         0                            0           0                          0;
    0                   t.kd*t.w0                0                   0                    -t.kd/(t.B_+t.b)  (t.A_+2*t.b)*t.w0/(t.B_+t.b)                   0   t.w0*(t.A_+t.a+t.b-t.c)/(t.B_+t.b);
    t.kd*t.w0                     0                     0                   0       -(t.A_+2*t.b)*t.w0/(t.B_+t.b)             -t.kd/(t.B_+t.b)     -t.w0*(t.A_+t.a+t.b-t.c)/(t.B_+t.b)     0;
    0                       0                     0                   0                        0                             0     -t.h0/t.A0                        0;
    0                       0                     0                   0                        0                             0          0                       -t.h0/t.B0  ];
A(:,3:4)=[];
A(3:4,:)=[];
niz=2;
dt=0.0005;
Tend=pi;
t=0:dt:Tend;
B=[zeros(4,2);eye(2)];
C=[eye(2) zeros(2,4)] ;
D=[];
C=[eye(niz) zeros(niz,6-niz)];
w0=4.5;w2=4;w3=5;ksi=0.9;
goal1=sort([desir(w0,ksi) desir(w2,ksi) desir(w3,ksi)]);
Gs=ss(A,B,C,0);
Q=[Umax^2 0; 0 Umax^2]; 
W=[Ma^2 0;0 Ma^2];
[kalmf,G,P,M] = kalman(Gs,W,Q);


P=place(A,B,goal1);
x=zeros(6,1);
xk=x;
xf=[];
xw=[];
u=[0;0];
for t=0:dt:Tend;
    w=[1 1]';    
    
    dx=A*x+B*u+B*w;   
    x=x+dx*dt;
    
    dxk=(A-G*C)*xk+G*C*x+B*u;   
    xk=xk+dxk*dt;
    
    u=-P*xk;
    
    xf=[xf xk];    
    xw=[xw C*x];
end

%w=[sin(0.2*t.*t); sin(0.2*t.*t)];
%Mysys=ss((A-B*P),B,C,0);
%xf=lsim(Mysys,w',t);
Xout=xf';
Yin=xw';
end


