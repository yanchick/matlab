t=0:0.01:60;

for pow=0.1:0.3:0.5
p= (t.^pow*90)/(60^pow);

for k=2:2:8
k1=k;%Количество слоев задержки по состоянию
k2=0;%Количество слоев задержки по состоянию
x=t+0.5*randn(1,length(t));;
y=p;
in=[];
out=[];
Q=[];

%%%%%%%%%
%Вход
%%%%%%%%%
for j=0:k1
n=length(x);
z=[zeros(1,j) x(1:n-j)];
in=[in; z];
Q=[Q ;min(x) max(x)];
end


%%%%%%%%%
%Выход
%%%%%%%%%
for j=1:k2
n=length(y);
zl=[zeros(1,j) y(1:n-j)];
out=[out; y];
Q=[Q ;min(y) max(y)];
end


%%%%%%%%
%Обучение
%%%%%%%%

net3=newff(Q,[5 1],{'tansig' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs = 5000;
net3.trainParam.goal = 1e-5;
net3.trainParam.min_grad=1e-100;
net3.trainParam.mu_max=1e100;
[net3]=train(net3,[in],y);k1=k1+1;


qw=[]; qqw=[];InNet1=zeros(k1,1);tin1=InNet1;
for i=0:0.01:60;
    z=i+4*rand(1);
    tin1=[z; tin1];    
    InNet1=tin1(1:k1);
   qw=[qw sim(net3,InNet1)];
   qqw=[qqw (z^0.3*90)/(60^0.3)];
end
file=strcat('neural_',num2str(pow),'_',num2str(k),'.cpp');
GenArduinoNN(net3,file)
end
end