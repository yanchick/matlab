function [alp,OutNet,ti]=NN3dem1(k,n,learn,gen)
%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Моделирование, две сети
%Скрипт точно работал 21-03-2010
%======================================================================

k1_1=k+1;%Количество слоев задержки по состоянию
k2_1=0;%Количество сл   оев задержки по упраавлению
k3=3;%Порядок системы

tic;
net3=NNcalc(k,n,learn,gen);
ti=toc;
dt=1e-3;
Tend=10;
x0=zeros(k3,1);
x=x0;
xz=x;
sqerr=[];sqerr2=[];

ksi=0.7;
w0=1*2*pi;
H=0.12;
A=0.016;
B=0.0005;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];
G=[1; 0 ;0];
C=eye(3);  
D=zeros(3,1);
w0=1;w1=3;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);

InNet1=zeros(k1_1,1);
InNet2=zeros(k2_1,1);
alpha=[0];
alphapr=0;
t1=0:dt:Tend;
u0=0;
xest=x0;
xwcap=[0;0;0];
Sqerr=[0;0;0];
  
for t=0:dt:Tend;
    w=1*rand(1);
    tin1=InNet1;
    tin2=InNet2;
    
    u=-P*xest;
    dx=F*x+G*u+G*w;   
    x=x+dx*dt;
    xz=[xz x];
    y=x(3);
    
    tin1=[y; tin1];
    tin2=[u; tin2];
    
    InNet1=tin1(1:k1_1);
    InNet2=tin2(1:k2_1);
    
    tin1=[];
    tin2=[];
    xest=sim(net3,[InNet1;InNet2]);
    Sqerr=[Sqerr sqrt((xest-x).^2)];

    sqerr=[sqerr xest];   
    alphapr=alphapr+x(1)*dt;
    alpha=[alpha; rad2deg(alphapr)*60];
end
alp=alpha;
OutNet=net3;
end

function net=NNcalc(k,n_ty,learn,gen)
%=======================================================================
%Объект третьего порядка с нейронным регулятором
%Расчет, одна сеть
%======================================================================
t=0:1e-3:7.5;
if (gen==1)
    w=0.1*randn(length(t),1)';
end
if (gen==2)
w=sin(1.5*2*pi*t);
end
if (gen==3)
    w=square(0.5*pi*t);
end
ksi=0.7;
w0=1*2*pi;
H=0.12;
A=0.016;
B=0.0005;
F=[0  -H/B 0;
   H/A   0 0;
   0     1 0;];
G=[1; 0 ;0];
C=eye(3);  
D=zeros(3,1);
w0=1;w1=3;ksi=0.9;
goal=sort([desir(w0,ksi) -1/w1]);
P=place(F,G,goal);
Mysys=ss((F-G*P),G,C,D);
xf=lsim(Mysys,w,t);
<<<<<<< HEAD
k1_1=k;%Количество слоев задержки по состоянию
=======
k1_1=k+1;%Количество слоев задержки по состоянию
%>>>>>>> 2e6279034f8faa6b672fba2dfc510a45771c6f91
k2_1=0;%Количество слоев задержки по упраавлению
k3=3;%Порядок системы
x=[];
Q=[];

%=============================
%Первая подсистема - гироблок
%=============================
y_target=xf;    
y=xf(:,3)';

<<<<<<< HEAD
for j=0:k1_1
=======
for j=0:k
>>>>>>> 2e6279034f8faa6b672fba2dfc510a45771c6f91
n=length(y);
z=[zeros(1,j) y(1:n-j)];
x=[x; z];
Q=[Q ;min(y) max(y)];
end

<<<<<<< HEAD
net3=newff(Q,[n 3],{'purelin' 'purelin'},'trainlm');
=======
ltp=[n_ty 3];
<<<<<<< HEAD
net3=newff(Q,ltp,{'tansig' 'purelin'},'trainlm');
>>>>>>> 2e6279034f8faa6b672fba2dfc510a45771c6f91
=======
net3=newff(Q,ltp,{'tansig' 'purelin'},learn);
>>>>>>> 377e757c2c4f77ef4a5ef1393c471e06da9bf0c6
net3.trainParam.show = 2;
net3.trainParam.epochs = 15000;
net3.trainParam.goal = 1e-5;
net3.trainParam.min_grad=1e-20;
net3=train(net3,x,y_target');
net=net3
end
