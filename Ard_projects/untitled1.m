%t=0:0.01:60;
% % t0=[];
%t0=t+randn(1,length(t));
% %
% % for i=1:length(t)
% % t0=[t0 t1(i)+1*randn(1)];
% % end
% %
% % t1=60:-0.01:0;
% % p0=(-1*exp(0.075*t));
% % p=(-1*exp(0.075*t)+max(abs(p0)))*60/max(abs(p0));

%for pp=2:20
t=0:0.01:60;
t0=t+randn(1,length(t));
myfun = inline('(-1*exp(t*T0)+exp(T0*t1))*60/exp(T0*t1)');
p=myfun(0.075,t,60);


k1=4;%Количество слоев задержки по состоянию
k2=0;%Количество слоев задержки по состоянию
x=t0;
y=p;
in=[];
out=[];
Q=[];
%t=0:0.01:2*pi
%

%%%%%%%%%
%Вход
%%%%%%%%%
for j=0:k1
n=length(x);
z=[zeros(1,j) x(1:n-j)];
in=[in; z];
Q=[Q ;min(x) max(x)];
end


%%%%%%%%%
%Выход
%%%%%%%%%
for j=1:k2
n=length(y);
zl=[zeros(1,j) y(1:n-j)];
out=[out; y];
Q=[Q ;min(y) max(y)];
end


%%%%%%%%
%Обучение
%%%%%%%%

net3=newff(Q,[5 1],{'tansig' 'purelin'},'trainlm');
net3.trainParam.show = 2;
net3.trainParam.epochs =2000;
net3.trainParam.goal = 27e-2;
net3.trainParam.min_grad=1e-100;
net3.trainParam.mu_max=1e100;
[net3 p]=train(net3,[in;out],y);k1=k1+1;

%RR{pp}.qq=[];RR{pp}.qqq=[];RR{pp}.qqqq=[];
qq=[];qqq=[];qqqq=[];

for k=1:3:50
z=[];zi=[];start=0;mend=60;step=0.03;l=1;
InNet=zeros(1,k1);InNett=zeros(1,k2);
i=start;

while(1)
  t=k+randn(1);
  InNet=[t InNet];
  InNet=InNet(1:k1);
  zi=[zi t];
  z=[z sim(net3,InNet')];
  i=i+step;
  if (l<4)
      if (i>=mend )
          t=start;
          start=mend;
          mend=t;
          step=-step;
          i=start;
          l=l+1;
      end
  else break;
  end
end
%   RR{pp}.qqqq=[RR{pp}.qqqq k];
%   RR{pp}.qq=[RR{pp}.qq std(z(1000:2000))];
%  RR{pp}.qqq=[RR{pp}.qqq std(zi)];
 qqqq=[qqqq k];
 qq=[qq std(z(1000:2000))];
 qqq=[qqq std(zi)];

end
 
% end